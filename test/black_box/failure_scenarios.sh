#!/bin/bash

TEST_DIR="test/data/BBT_intended_failure"
TESTS=$(ls $TEST_DIR | tr '\n' ' ')

FAILURES=0

EXEC_PATH="$(pwd)/pickaxe"
for TEST in $TESTS; do
  TEST_PATH=$TEST_DIR/$TEST
  pushd $TEST_PATH > /dev/null
  COMMAND="$EXEC_PATH -config=$(pwd)/config.yaml"
  $COMMAND > /dev/null 2>&1
  if [ $? == 0 ]; then
    echo "Test: $TEST"
    echo "Command '$COMMAND' didn't fail as expected"
    echo ""
    FAILURES=1
  fi
  if cat output.csv > /dev/null 2>&1; then
    echo "Test: $TEST"
    echo "Command '$COMMAND' had output when none was expected:"
    cat output.csv
    rm output.csv > /dev/null
    echo ""
    FAILURES=1
  fi
  popd > /dev/null
done

exit $FAILURES
