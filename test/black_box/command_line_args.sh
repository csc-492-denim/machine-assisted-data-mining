#!/bin/bash

FAILURES=0

ROW_LENGTH=3

TEST_DATA=(
  "./pickaxe -wat"                              2 "Expected exit code = 2 for non-existent flag"
  "./pickaxe -config=example_config.yaml"       0 "Expected zero exit code for correct usage of -config flag"
  "./pickaxe"                                   1 "Expected exit code = 1 for empty -config"
  "./pickaxe -config=test/data/bad_config.yaml" 1 "Expected exit code = 1 for using directory as input file"
)

NUM_TESTS=$(( ${#TEST_DATA[@]} / $ROW_LENGTH))

for (( i=0; i<$NUM_TESTS; i++ )); do
  COMMAND=${TEST_DATA[$i*$ROW_LENGTH]}
  EXPECTED_RETURN_CODE=${TEST_DATA[i*$ROW_LENGTH + 1]}
  ERROR_MESSAGE=${TEST_DATA[i*ROW_LENGTH + 2]}

  $COMMAND > /dev/null 2>&1
  if [ ! $? == $EXPECTED_RETURN_CODE ]; then
    echo "Test: $COMMAND"
    echo $ERROR_MESSAGE
    echo ""
    FAILURES=1
  fi
done

exit $FAILURES
