#!/bin/bash

TEST_DIR="test/data/BBT"
TESTS=$(ls $TEST_DIR | tr '\n' ' ')

FAILURES=0

EXEC_PATH="$(pwd)/pickaxe"
for TEST in $TESTS; do
  TEST_PATH=$TEST_DIR/$TEST
  pushd $TEST_PATH > /dev/null
  COMMAND="$EXEC_PATH -config=$(pwd)/config.yaml"
  $COMMAND > /dev/null 2>&1
  if [ ! $? == 0 ]; then
    echo "Test: $TEST"
    echo "Command '$COMMAND' failed to run."
    echo ""
    FAILURES=1
  fi
  if ! diff output.csv expected.csv > /dev/null; then
    echo "Test: $TEST"
    echo "Command '$COMMAND' had incorrect output."
    echo ""
    FAILURES=1
  fi
  popd > /dev/null
done

exit $FAILURES
