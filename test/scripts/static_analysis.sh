#!/bin/bash

source /etc/profile.d/go.sh

BAD_FILE_COUNT=$(gofmt -l ./ | wc -l)

if [ ! $BAD_FILE_COUNT == "0" ]; then
  gofmt -d -e ./src/
  echo "Please fix these formatting errors"
  exit 1
fi

go vet ./src/...

if [ ! $? -eq 0 ]; then
  echo "Please fix these errors"
  exit 1
fi

go get -u github.com/golang/lint/golint
golint -set_exit_status ./src/...

if [ ! $? -eq 0 ]; then
  echo "Please look at the above formatting errors"
fi
