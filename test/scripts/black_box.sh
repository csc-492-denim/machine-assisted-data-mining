#!/bin/bash

source /etc/profile.d/go.sh

# Install all go dependencies
go get -d -t ./...

FAILURES=0

go build src/pickaxe.go

tests=$(ls test/black_box/*.sh)

for test in ${tests[@]}; do
  $test
  RET=$?
  if [ ! $RET -eq 0 ]; then
    FAILURES=$RET
  fi
done

exit $FAILURES
