#!/bin/bash

set -u

COVERAGE_MINIMUM=70

source /etc/profile.d/go.sh
FAILURES=0
rm -rf coverage/ || /bin/true
mkdir coverage

# Install all go dependencies
go get -d -t ./...

modules=( "controller" 
          "convert" 
          "config" 
          "validate" 
          "utils" 
          "load" 
          "export" 
          "restructure"
          "log" )

echo "<html><head></head><body>" >> coverage/index.html
echo "<h1>Coverage report for each module</h1>" >> coverage/index.html

for module in "${modules[@]}"
do
  OUTPUT=$(go test -coverprofile=coverage/$module.out machine-assisted-data-mining/src/$module)
  RET=$?
  echo $OUTPUT
  if [ ! $RET -eq 0 ]; then
    FAILURES=$RET
  fi
  coverage=$(echo $OUTPUT | cut -d':' -f2 | grep -o -E '[0-9.]+' | cut -d'.' -f1)
  if [ $coverage -lt $COVERAGE_MINIMUM ]; then
    FAILURES=1
    echo "Module $module failed coverage check.  Has $coverage% coverage, needs $COVERAGE_MINIMUM%."
  fi
  go tool cover -html=coverage/$module.out -o coverage/$module.html
  echo "<a href=\"$module.html\">$module</a><br>" >> coverage/index.html
done

echo "</body></html>" >> coverage/index.html

rm -f coverage/*.out

exit $FAILURES
