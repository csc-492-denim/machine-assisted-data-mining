#!/bin/bash

GO_VERSION=${GO_VERSION:-1.10.1}
OS=linux
ARCH=amd64

if which yum; then
  PACKAGE_MANAGER=yum
else
  PACKAGE_MANAGER=apt-get
fi

if [ $PACKAGE_MANAGER == "apt-get" ]; then
  apt-get update
  apt-get -y install wget tar git make gcc libreadline-dev
else
  yum install -y wget tar git make gcc readline readline-devel
fi

if ! /usr/local/go/bin/go version | grep $GO_VERSION; then
    rm -rf /usr/local/go || /bin/true
    GO_INSTALL_FILE=go$GO_VERSION.$OS-$ARCH.tar.gz
    wget --quiet https://dl.google.com/go/$GO_INSTALL_FILE
    tar -C /usr/local -xzf $GO_INSTALL_FILE
    rm -f $GO_INSTALL_FILE
fi

grep $GO_BIN_DIR $GO_VARS_FILE || echo "export PATH=\$PATH:$GO_BIN_DIR" >> $GO_VARS_FILE

chmod 0777 $GO_VARS_FILE
