#!/bin/bash

# Remove possible symlinks
# This uninstalls our project
rm /usr/local/go/src/machine-assisted-data-mining
rm $HOME/go/src/machine-assisted-data-mining


# Remove rosie installation
# List of possible places rosie could be installed
dirs=( "/usr/local/go/src/rosie-pattern-language" "$HOME/go/src/rosie-pattern-language" )
for dir in "${dirs[@]}"
do
  if [ -d $dir ]; then
    pushd $dir
    make uninstall
    popd
    rm -rf $dir
  fi
done
