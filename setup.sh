#!/bin/bash

set -eux

GO_VARS_FILE="/etc/profile.d/go.sh"
GO_BIN_DIR="/usr/local/go/bin"

PRIV_COMMAND="GO_VARS_FILE=$GO_VARS_FILE GO_BIN_DIR=$GO_BIN_DIR ./setup/privileged_setup.sh"
if [ ! $(id -u) == 0 ]; then
  sudo -E sh -c "$PRIV_COMMAND"
else
  sh -c "$PRIV_COMMAND"
fi

HOME=$(echo ~)
GO_LIB_DIR="$HOME/go/src"
USER_GO_BIN_DIR="$HOME/go/bin"
grep $USER_GO_BIN_DIR $GO_VARS_FILE || echo "export PATH=\$PATH:$USER_GO_BIN_DIR" >> $GO_VARS_FILE

GOPATH="$HOME/go"
grep GOPATH $GO_VARS_FILE || echo "export GOPATH=$GOPATH" >> $GO_VARS_FILE

source $GO_VARS_FILE

if [ ! -d $GO_LIB_DIR ]; then
  mkdir -p $GO_LIB_DIR
fi

PROJECT_DIR=$(basename `pwd`)
# Symlinks our source code into the GOPATH.
if [ ! -d $GO_LIB_DIR/$PROJECT_DIR ]; then
  ln -s `pwd` $GO_LIB_DIR/
fi

if [ ! -d $GO_LIB_DIR/rosie-pattern-language ]; then
  pushd $GO_LIB_DIR
  git clone https://github.com/jamiejennings/rosie-pattern-language.git
  popd
else
  pushd $GO_LIB_DIR/rosie-pattern-language
  git checkout master
  git pull
  popd
fi

pushd $GO_LIB_DIR/rosie-pattern-language
git fetch --tags
git checkout tags/v1.0.0-beta-8
make LUADEBUG=1
CLIENTS=go make test
popd

# Install all go dependencies
go get -d -t ./...

echo "SUCCESS!"
echo "Go Version: $(/usr/local/go/bin/go version)"
echo "Rosie Version: $($GO_LIB_DIR/rosie-pattern-language/bin/rosie version)"
