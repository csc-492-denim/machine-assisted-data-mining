# Machine Assisted Data Mining

[![Build status](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/build.svg)](https://gitlab.com/csc-492-denim/machine-assisted-data-mining/commits/master)

[Coverage Reports](https://csc-492-denim.gitlab.io/machine-assisted-data-mining/index.html)

This project is being hosted on
[GitLab](https://gitlab.com/csc-492-denim/machine-assisted-data-mining).
All of our issues, tasks, merge requests,
and other such documentation will be publicly available there.

## Team Members
* Edward Chan
* Nicole Hlebak
* Andy Zhang
* Jean-Claude Shore


## Installation

To install Go and Rosie on your system and build the Rosie Go client, run the `setup.sh` script under your regular user account.

NOTE: DO NOT run `setup.sh` under `sudo`.  It will install things in a directory not accessible as your user!  At one point the script will
prompt you for your password to execute a few commands under `sudo`, which is intended.

```bash
# Do not run with sudo
./setup.sh
```

This will install Go and Rosie and make both accessible in the system `PATH`.  It will also build the Go client in the project and add it to the GOPATH.  Then, you can access it within a Go file with:

```Go
import (
    "rosie-pattern-language/librosie/src/go/src/rosie"
)
```

You can also access this project's modules:
```Go
import (
    "machine-assisted-data-mining/controller"
)
```

To clean up a previous setup and re-install (such as after a breaking change in the setup script):

```bash
sudo ./un_setup.sh
# Do not run with sudo
./setup.sh
```

## Testing

To run the tests, make sure you are in the top-level project directory (the one with this file).  Then, execute the test file you would like to run:

```bash
./test_scripts/white_box.sh
./test_scripts/black_box.sh
```

To see code coverage, add the following to your VSCode settings:

```json
"go.coverOnSave": true
```

For some reason, it won't show the covered lines otherwise (at least for me).  This does mean that every time you save a file, it runs the unit tests related to that file.
