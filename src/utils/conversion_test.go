package utils

import (
	"reflect"
	"strings"
	"testing"
)

func TestConvertToStringMap(t *testing.T) {
	input := map[interface{}]interface{}{
		"key1": "value1",
		"key2": "value2",
	}
	expected := map[string]string{
		"key1": "value1",
		"key2": "value2",
	}
	output, err := ConvertToStringMap(input)
	if err != nil {
		t.Errorf("Unexpected error: %s", err)
	}

	if !(reflect.TypeOf(expected) == reflect.TypeOf(output)) {
		t.Errorf("Type of output incorrect.  Actual: %v", reflect.TypeOf(output))
	}
	if !reflect.DeepEqual(expected, output) {
		t.Errorf("Output did not match.  Expected: %v  Actual: %v", expected, output)
	}
}

func TestConvertToStringMapBadKey(t *testing.T) {
	input := map[interface{}]interface{}{
		1:      "value1",
		"key2": "value2",
	}
	output, err := ConvertToStringMap(input)
	if output != nil {
		t.Errorf("Expected nil output.  Got: %v", output)
	}
	if !strings.Contains(err.Error(), "Non-string in map") {
		t.Errorf("Error did not contain expected string.")
	}
}

func TestConvertToStringMapBadValue(t *testing.T) {
	input := map[interface{}]interface{}{
		"key1": 1,
		"key2": "value2",
	}
	output, err := ConvertToStringMap(input)
	if output != nil {
		t.Errorf("Expected nil output.  Got: %v", output)
	}
	if !strings.Contains(err.Error(), "Non-string in map") {
		t.Errorf("Error did not contain expected string.")
	}
}

func TestConvertToStringList(t *testing.T) {
	input := []interface{}{
		"value1",
		"value2",
	}
	expected := []string{
		"value1",
		"value2",
	}
	output, err := ConvertToStringList(input)
	if err != nil {
		t.Errorf("Unexpected error: %s", err)
	}

	if !(reflect.TypeOf(expected) == reflect.TypeOf(output)) {
		t.Errorf("Type of output incorrect.  Actual: %v", reflect.TypeOf(output))
	}
	if !reflect.DeepEqual(expected, output) {
		t.Errorf("Output did not match.  Expected: %v  Actual: %v", expected, output)
	}
}

func TestConvertToStringListBadValue(t *testing.T) {
	input := []interface{}{
		1,
		"value2",
	}
	output, err := ConvertToStringList(input)
	if output != nil {
		t.Errorf("Expected nil output.  Got: %v", output)
	}
	if !strings.Contains(err.Error(), "Non-string in list") {
		t.Errorf("Error did not contain expected string.")
	}
}
