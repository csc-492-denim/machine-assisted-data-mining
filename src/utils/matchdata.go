package utils

import (
	"rosie-pattern-language/src/librosie/go/src/rosie"
	"strings"
)

type MatchData struct {
	Data  string
	Type  string
	Start int
	End   int
	Subs  []MatchData
}

// CreateMatchData traverses a rosie Match and returns a MatchData data structure
func CreateMatchData(match *rosie.Match) *MatchData {
	if match != nil {
		ptr := extractMatchData(match.Data)
		return &ptr
	}
	return nil
}

// recursively extract data from json data structure into a MatchData
func extractMatchData(json map[string]interface{}) MatchData {
	data := MatchData{}

	if json["data"] != nil {
		data.Data = json["data"].(string)
	}

	if json["type"] != nil {
		data.Type = json["type"].(string)
	}
	if json["e"] != nil {
		data.End = int(json["e"].(float64))
	}
	if json["s"] != nil {
		data.Start = int(json["s"].(float64))
	}

	if subs, ok := json["subs"]; ok {
		data.Subs = make([]MatchData, len(subs.([]interface{})))

		for i, s := range subs.([]interface{}) {
			data.Subs[i] = extractMatchData(s.(map[string]interface{}))
		}
	}

	return data
}

func (m MatchData) Search(typename string) []MatchData {
	results := []MatchData{}
	return m.searchByType([]string{typename}, results)
}

func (m MatchData) SearchMany(typenames ...string) []MatchData {
	results := []MatchData{}
	return m.searchByType(typenames, results)
}

func (m MatchData) searchByType(typenames []string, matches []MatchData) []MatchData {
	for _, tn := range typenames {
		if m.Type == tn {
			matches = append(matches, m)
			break
		}
	}

	for _, sub := range m.Subs {
		matches = sub.searchByType(typenames, matches)
	}

	return matches
}

func (m MatchData) SearchContains(typename string) []MatchData {
	results := []MatchData{}
	return m.searchByTypeContains([]string{typename}, results)
}

func (m MatchData) SearchManyContains(typenames ...string) []MatchData {
	results := []MatchData{}
	return m.searchByTypeContains(typenames, results)
}

func (m MatchData) searchByTypeContains(typenames []string, matches []MatchData) []MatchData {
	for _, tn := range typenames {
		if strings.Contains(m.Type, tn) {
			matches = append(matches, m)
			break
		}
	}

	for _, sub := range m.Subs {
		matches = sub.searchByTypeContains(typenames, matches)
	}

	return matches
}
