package utils

import (
	"fmt"
	"rosie-pattern-language/src/librosie/go/src/rosie"
	"sync"
)

var rosieEngineMutex *sync.Mutex

func init() {
	rosieEngineMutex = &sync.Mutex{}
}

// NewRosieEngine initializes a new rosie engine with the given package
func NewRosieEngine(pkgs ...string) (*rosie.Engine, error) {
	rosieEngineMutex.Lock()
	engine, err := rosie.New("Restructure Engine")
	rosieEngineMutex.Unlock()
	if err != nil {
		return nil, fmt.Errorf("Can not start Rosie engine %s", err)
	}

	for _, pkg := range pkgs {
		ok, _, _, err := engine.ImportPkg(pkg)
		if err != nil || !ok {
			return nil, fmt.Errorf("Can not load in pkg in Rosie %s", err)
		}
	}

	return engine, nil
}
