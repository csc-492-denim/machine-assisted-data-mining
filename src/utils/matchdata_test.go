package utils

import (
	"encoding/json"
	"reflect"
	"testing"
)

func extractAndCompare(t *testing.T, testNum int, jsonText string, expectedOutput MatchData) {
	t.Logf("Test %v of extractMatchData\n", testNum)
	var jsonMap map[string]interface{}

	if err := json.Unmarshal([]byte(jsonText), &jsonMap); err != nil {
		panic(err)
	}

	actualOutput := extractMatchData(jsonMap)

	if !reflect.DeepEqual(expectedOutput, actualOutput) {
		t.Errorf("Test %v failed, expected %v but got %v\n", testNum, expectedOutput, actualOutput)
	}
}

func TestExtractMatchData(t *testing.T) {
	json1 := `{"s":0, "e":1, "type":"num.int", "data":"1"}`
	expectedOutput := MatchData{"1", "num.int", 0, 1, nil}
	extractAndCompare(t, 1, json1, expectedOutput)

	json2 := `{"type":"all.things","s":1,"subs":[{"type":"all.thing","s":1,"subs":[{"type":"all.punct","s":1,"e":2,"data":"#"}],"e":2,"data":"#"}],"e":2,"data":"#"}`
	expectedOutput = MatchData{"#", "all.things", 1, 2, []MatchData{MatchData{"#", "all.thing", 1, 2, []MatchData{MatchData{"#", "all.punct", 1, 2, nil}}}}}
	extractAndCompare(t, 2, json2, expectedOutput)
}

func TestSearch(t *testing.T) {
	testMatchData := MatchData{"1", "num.int", 0, 1, nil}
	expectedOutput := testMatchData

	actualOutput := testMatchData.Search("num.int")
	if !reflect.DeepEqual(expectedOutput, actualOutput[0]) {
		t.Errorf("Search test failed, expected %v but got %v\n", expectedOutput, actualOutput)
	}

	actualOutput = testMatchData.SearchContains("num.")
	if !reflect.DeepEqual(expectedOutput, actualOutput[0]) {
		t.Errorf("Search test failed, expected %v but got %v\n", expectedOutput, actualOutput)
	}
}
