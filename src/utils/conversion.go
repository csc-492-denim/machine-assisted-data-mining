package utils

import "fmt"

// ConvertToStringMap converts a map of interfaces to strings
func ConvertToStringMap(input map[interface{}]interface{}) (output map[string]string, err error) {
	output = make(map[string]string)
	for key, val := range input {
		keyString, ok := key.(string)
		if !ok {
			return nil, fmt.Errorf("Non-string in map")
		}
		valString, ok := val.(string)
		if !ok {
			return nil, fmt.Errorf("Non-string in map")
		}
		output[keyString] = valString
	}
	return output, nil
}

// ConvertToStringList converts a list of interfaces to strings
func ConvertToStringList(input []interface{}) (output []string, err error) {
	for _, val := range input {
		valString, ok := val.(string)
		if !ok {
			return nil, fmt.Errorf("Non-string in list")
		}
		output = append(output, valString)
	}
	return output, nil
}
