package utils

/*
	Each Row of data
*/
type Row struct {
	Data       []string
	Errors     []error
	LineNumber int
	Deleted    bool
}

// AddError adds an error to this Row's slice of errors
func (r *Row) AddError(err error) {
	r.Errors = append(r.Errors, err)
}
