package utils

import (
	"strings"
	"testing"
)

func TestNewRosieEngine(t *testing.T) {
	engine, err := NewRosieEngine("all")
	if engine == nil {
		t.Errorf("Engine was nil")
	}
	if err != nil {
		t.Errorf("Unexpected error when creating engine")
	}
}

func TestNewRosieEngineMultiplePackages(t *testing.T) {
	packages := []string{"all", "num"}
	engine, err := NewRosieEngine(packages...)
	if engine == nil {
		t.Errorf("Engine was nil")
	}
	if err != nil {
		t.Errorf("Unexpected error when creating engine")
	}
}

func TestNewRosieEngineBadPackage(t *testing.T) {
	engine, err := NewRosieEngine("thisdoesn'texist")
	if engine != nil {
		t.Errorf("Expected nil engine")
	}
	if err == nil {
		t.Errorf("Expected non-nil error")
		return
	}
	if !strings.Contains(err.Error(), "Can not load in pkg") {
		t.Errorf("Error did not contain expected string.  Actual: ")
	}
}
