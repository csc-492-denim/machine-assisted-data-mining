package config

import (
	"io/ioutil"

	yaml "gopkg.in/yaml.v2"
)

// Task is a struct that has an operation and any args it requires
type Task struct {
	Operation string
	Args      map[string]interface{}
}

// Config stores the config for the running program
type Config struct {
	Tasks []Task `yaml:"config"`
}

// Load loads a config file from disk and returns a corresponding data structure
func Load(path string) (Config, error) {
	configBytes, err := ioutil.ReadFile(path)
	if err != nil {
		return Config{}, err
	}
	var config Config
	yaml.Unmarshal(configBytes, &config)
	return config, nil
}
