package config

import (
	"reflect"
	"testing"
)

func TestLoad(t *testing.T) {
	config, err := Load("test_data/config.yaml")
	if err != nil {
		t.Errorf("Unexpected error loading config: %s", err)
	}
	expected := Config{
		Tasks: []Task{
			Task{
				Operation: "Load",
				Args: map[string]interface{}{
					"path": "input.csv",
				},
			},
			Task{
				Operation: "Convert",
				Args: map[string]interface{}{
					"column":   "all",
					"function": "IntToFloat",
					"type":     "num.int",
				},
			},
			Task{
				Operation: "Export",
				Args: map[string]interface{}{
					"path": "output.csv",
				},
			},
		},
	}
	if !reflect.DeepEqual(config, expected) {
		t.Errorf("Expected didn't match actual config.  Expected: %v  Actual: %v", expected, config)
	}
}

func TestBadConfig(t *testing.T) {
	_, err := Load("test_data/bad_config.yaml")
	if err == nil {
		t.Errorf("Expected error when loading non-existent config file.")
	}
}
