package restructure

import (
	"fmt"
	"machine-assisted-data-mining/src/utils"
	"rosie-pattern-language/src/librosie/go/src/rosie"
	"strconv"
)

// Config stores the config for a restructure operation
type Config struct {
	Pattern      string
	Variables    map[string]string
	Dependencies []string
}

func (c Config) constructCode() string {
	code := ""

	for k, v := range c.Variables {
		code = code + k + " = " + v + "\n"
	}

	return code
}

// New restructurer using a Row
func New(input []utils.Row) Restructurer {
	return Restructurer{rows: input}
}

// Restructurer has rows
type Restructurer struct {
	rows []utils.Row
}

// inserts a string into a slice of strings
func insert(row utils.Row, index int, value string) []string {
	row.Data = append(row.Data, "")
	copy(row.Data[index+1:], row.Data[index:])
	row.Data[index] = value
	return row.Data
}

// Operate takes data and a config and restructures data based on the config
func (r Restructurer) Operate(config Config, column string) (err error) {
	engine, err := utils.NewRosieEngine(config.Dependencies...)
	if err != nil {
		return fmt.Errorf("Could not start Rosie engine %s", err)
	}

	_, _, _, err = engine.LoadString(config.constructCode())
	if err != nil {
		return fmt.Errorf("LoadString failed: %s", err)
	}

	pattern, _, err := engine.Compile(config.Pattern)
	if err != nil {
		return fmt.Errorf("Could not compile extract pattern %s", err)
	}

	i := 0
	vars := make([]string, len(config.Variables))
	for k := range config.Variables {
		vars[i] = k
		i++
	}

	for i, row := range r.rows {
		if row.Deleted {
			continue
		}
		r.rows[i], err = restructureRow(pattern, row, vars, column)
		if err != nil {
			row.AddError(err)
		}
	}

	return nil
}

func restructureRow(pattern *rosie.Pattern, row utils.Row, vars []string, column string) (output utils.Row, err error) {
	stop := len(row.Data)
	start := 0
	if column != "all" {
		start, err = strconv.Atoi(column)
		if err != nil {
			return output, err
		}
		//start = temp //scope issues require a temp variable
		stop = start + 1
	}
	for i := start; i < stop; i++ {
		col := row.Data[i]
		match, err := pattern.MatchString(col)

		if err != nil {
			return output, fmt.Errorf("error matching pattern %s", err)
		} else if len(match.Data) > 0 {
			matchData := utils.CreateMatchData(match)
			searchData := matchData.SearchMany(vars...)

			defer func(i int) {
				// Append re-structured data to end of structure, this way
				// we don't mess up headers.
				length := len(row.Data)
				for j, m := range searchData {
					output.Data = insert(output, length+j, m.Data)
				}
			}(i)
		} else {
			row.AddError(fmt.Errorf("restructure: no match for column %v: %v", i, row.Data[i]))
		}

	}
	return row, nil
}
