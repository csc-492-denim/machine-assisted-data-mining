package restructure

import (
	"errors"
	"machine-assisted-data-mining/src/utils"
	"reflect"
	"testing"
)

func TestSplitOrderedPair(t *testing.T) {
	input := []utils.Row{utils.Row{Data: []string{"(1,2)", "(3,4)"}, Errors: []error{}},
		utils.Row{Data: []string{"(15,2912)", "325"}, Errors: []error{}}}

	expectedOutput := []utils.Row{utils.Row{Data: []string{"(1,2)", "(3,4)", "1", "2", "3", "4"}, Errors: []error{}},
		utils.Row{Data: []string{"(15,2912)", "325", "15", "2912"}, Errors: []error{errors.New("restructure: no match for column 1: 325")}}}

	restructurer := New(input)
	numMap := map[string]string{"number": "num.any"}
	config := Config{Pattern: "[(] number [,] number [)]", Variables: numMap, Dependencies: []string{"num"}}
	err := restructurer.Operate(config, "all")

	if err != nil {
		t.Errorf("Failed, unexected error %s", err)
	}

	if !reflect.DeepEqual(expectedOutput, restructurer.rows) {
		t.Errorf("Failed, expected %v but got %v\n", expectedOutput, restructurer.rows)
	}
}

func TestSplitOrderedPairSingleColumn(t *testing.T) {
	input := []utils.Row{utils.Row{Data: []string{"(1,2)", "(3,4)"}, Errors: []error{}},
		utils.Row{Data: []string{"(15,2912)", "325"}, Errors: []error{}}}

	expectedOutput := []utils.Row{utils.Row{Data: []string{"(1,2)", "(3,4)", "1", "2"}, Errors: []error{}},
		utils.Row{Data: []string{"(15,2912)", "325", "15", "2912"}, Errors: []error{}}}

	restructurer := New(input)
	numMap := map[string]string{"number": "num.any"}
	config := Config{Pattern: "[(] number [,] number [)]", Variables: numMap, Dependencies: []string{"num"}}
	err := restructurer.Operate(config, "0")

	if err != nil {
		t.Errorf("Failed, unexected error %s", err)
	}

	if !reflect.DeepEqual(expectedOutput, restructurer.rows) {
		t.Errorf("Failed, expected %v but got %v\n", expectedOutput, restructurer.rows)
	}
}

func TestExtract(t *testing.T) {
	input := []utils.Row{utils.Row{Data: []string{"2", "15 hello"}, Errors: []error{}},
		utils.Row{Data: []string{"(15,2912)", "bam"}, Errors: []error{}}}

	expectedOutput := []utils.Row{utils.Row{Data: []string{"2", "15 hello", "2", "15", "hello"}, Errors: []error{}},
		utils.Row{Data: []string{"(15,2912)", "bam", "15", "2912", "bam"}, Errors: []error{}}}

	restructurer := New(input)
	numMap := map[string]string{"number": "num.any", "lilword": "[a-z]+"}
	config := Config{Pattern: "findall:{number / lilword}", Variables: numMap, Dependencies: []string{"num"}}
	err := restructurer.Operate(config, "all")

	if err != nil {
		t.Errorf("Failed, unexected error %s", err)
	}

	if !reflect.DeepEqual(expectedOutput, restructurer.rows) {
		t.Errorf("Failed, expected %v but got %v\n", expectedOutput, restructurer.rows)
	}
}

func TestExtractMedical(t *testing.T) {

	// Binge drinking thing test

	input := []utils.Row{utils.Row{Data: []string{"Binge drinking prevalence among adults aged >= 18 years", "Other stuff"}, Errors: []error{}},
		utils.Row{Data: []string{"Binge drinking prevalence among children aged < 18 years", "Other stuff"}, Errors: []error{}},
		utils.Row{Data: []string{"Casual alcohol consumption among elderly aged >= 65 years", "More other stuff"}, Errors: []error{}}}

	expectedOutput := []utils.Row{utils.Row{Data: []string{"Binge drinking prevalence among adults aged >= 18 years", "Other stuff", "Binge drinking prevalence", "adults", ">=", "18"}, Errors: []error{}},
		utils.Row{Data: []string{"Binge drinking prevalence among children aged < 18 years", "Other stuff", "Binge drinking prevalence", "children", "<", "18"}, Errors: []error{}},
		utils.Row{Data: []string{"Casual alcohol consumption among elderly aged >= 65 years", "More other stuff", "Casual alcohol consumption", "elderly", ">=", "65"}, Errors: []error{}}}

	variableMap := map[string]string{"PROBLEM": "(!\"among\" & word.any)+",
		"POPULATION": "word.any",
		"RELATION":   "[>=<]{1,2}",
		"AGE":        "num.int"}

	restructurer := New(input)
	pattern := "PROBLEM \"among\" POPULATION \"aged\" RELATION AGE \"years\""

	config := Config{Pattern: pattern, Variables: variableMap, Dependencies: []string{"word", "num"}}

	err := restructurer.Operate(config, "0")

	if err != nil {
		t.Errorf("Failed, unexected error %s", err)
	}

	if !reflect.DeepEqual(expectedOutput, input) {
		t.Errorf("Failed, expected %v but got %v\n", expectedOutput, input)
	}
}
