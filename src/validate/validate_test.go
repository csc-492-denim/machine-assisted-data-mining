package validate

import (
	"errors"
	"machine-assisted-data-mining/src/utils"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestCSV tests that various CSV files are correctly validated.
func TestDelete(t *testing.T) {

	// Check if all error
	input := []utils.Row{}
	errs := []error{errors.New("TestError")}
	noerrs := []error{}
	//check if some error
	input = append(input, utils.Row{Data: []string{"1", "2", "3"}, Errors: errs, LineNumber: 0})
	input = append(input, utils.Row{Data: []string{"a", "b", "c"}, Errors: noerrs, LineNumber: 1})
	input = append(input, utils.Row{Data: []string{"1", "2", "3"}, Errors: errs, LineNumber: 2})
	input = append(input, utils.Row{Data: []string{"a", "b", "c"}, Errors: noerrs, LineNumber: 3})

	validator := New(&input)

	validator.ValidateError("Delete")

	expected := []utils.Row{
		utils.Row{
			Data: []string{"1", "2", "3"},
			Errors: []error{
				errors.New("TestError"),
			},
			LineNumber: 0,
			Deleted:    true,
		},
		utils.Row{
			Data:       []string{"a", "b", "c"},
			Errors:     []error{},
			LineNumber: 1,
		},
		utils.Row{
			Data: []string{"1", "2", "3"},
			Errors: []error{
				errors.New("TestError"),
			},
			LineNumber: 2,
			Deleted:    true,
		},
		utils.Row{
			Data:       []string{"a", "b", "c"},
			Errors:     []error{},
			LineNumber: 3,
		},
	}

	if !reflect.DeepEqual(input, expected) {
		t.Errorf("Validate test failed.  Expected: %v, Actual: %v", expected, input)
	}
}

func TestPrint(t *testing.T) {
	errs := []error{errors.New("TestError")}
	noerrs := []error{}
	input := []utils.Row{}
	input = append(input, utils.Row{Data: []string{"1", "2", "3"}, Errors: errs, LineNumber: 0})
	input = append(input, utils.Row{Data: []string{"a", "b", "c"}, Errors: noerrs, LineNumber: 1})
	input = append(input, utils.Row{Data: []string{"1", "2", "3"}, Errors: errs, LineNumber: 2})
	input = append(input, utils.Row{Data: []string{"a", "b", "c"}, Errors: noerrs, LineNumber: 3})

	validator := New(&input)
	validator.ValidateError("Print")
}

func TestValidatePattern(t *testing.T) {
	noerrs := []error{}
	input := []utils.Row{}
	input = append(input, utils.Row{Data: []string{"(1, 2)", "2 test", "3 test"}, Errors: noerrs, LineNumber: 0})
	input = append(input, utils.Row{Data: []string{"a", "b", "c"}, Errors: noerrs, LineNumber: 1})
	input = append(input, utils.Row{Data: []string{"1", "2", "3"}, Errors: noerrs, LineNumber: 2})
	input = append(input, utils.Row{Data: []string{"a", "b", "c"}, Errors: noerrs, LineNumber: 3})
	Validator := New(&input)

	err := Validator.ValidatePattern(0, "Delete", "^num.int$", []string{"num", "all"})
	assert.Equal(t, nil, err)

	expected := []utils.Row{
		utils.Row{
			Data: []string{"(1, 2)", "2 test", "3 test"},
			Errors: []error{
				errors.New("Pattern does not match"),
			},
			LineNumber: 0,
			Deleted:    true,
		},
		utils.Row{
			Data: []string{"a", "b", "c"},
			Errors: []error{
				errors.New("Pattern does not match"),
			},
			LineNumber: 1,
			Deleted:    true,
		},
		utils.Row{
			Data:       []string{"1", "2", "3"},
			Errors:     []error{},
			LineNumber: 2,
			Deleted:    false,
		},
		utils.Row{
			Data: []string{"a", "b", "c"},
			Errors: []error{
				errors.New("Pattern does not match"),
			},
			LineNumber: 3,
			Deleted:    true,
		},
	}

	if !reflect.DeepEqual(input, expected) {
		t.Errorf("Validate test failed.  Expected: %v, Actual: %v", expected, input)
	}
}
