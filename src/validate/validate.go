package validate

import (
	"errors"
	"fmt"
	"machine-assisted-data-mining/src/utils"
	"reflect"
)

//Lambda body for all the actions
type action func(*[]utils.Row)

// Validator is the object return to help validate rows
type Validator struct {
	Rows *[]utils.Row
}

type validateFunctionHolder struct{}

// New return a pointer to a new validator
func New(input *[]utils.Row) *Validator {
	return &Validator{Rows: input}
}

//ValidateError validates the data slice and delete badly formatted rows
func (v *Validator) ValidateError(act string) error {
	v.callOperationWithReflection(act)
	return nil
}

// ValidatePattern used for validating a single column against a pattern
func (v *Validator) ValidatePattern(colIndex int, act string, pat string, dep []string) error {
	engine, err := utils.NewRosieEngine(dep...)
	if err != nil {
		return err
	}
	pattern, _, err := engine.Compile(pat)
	rows := *v.Rows
	for i := 0; i < len(rows); i++ {
		row := &rows[i]
		if row.Deleted {
			continue
		}
		col := row.Data[colIndex]
		match, err := pattern.MatchString(col)
		//if matches
		if err != nil {
			p := &(row.Errors)
			*p = append(*p, err)
		} else if len(match.Data) < 1 {
			p := &(row.Errors)
			*p = append(*p, errors.New("Pattern does not match"))
		}
	}
	v.callOperationWithReflection(act)
	return nil
}

//Delete check if the error list is empty, if not. Delete
func (v *validateFunctionHolder) Delete(rows []utils.Row) []utils.Row {
	for i := 0; i < len(rows); i++ {
		row := &rows[i]
		if len(row.Errors) != 0 {
			row.Deleted = true
		}
	}
	return rows
}

// Print check if the error list is empty, if not, print to stderr
func (v *validateFunctionHolder) Print(rows []utils.Row) []utils.Row {
	for i := 0; i < len(rows); i++ {
		row := &rows[i]
		if len(row.Errors) != 0 {
			fmt.Println(row.Data)
			fmt.Println(row.Errors)
		}
	}
	return rows
}

func (v *Validator) callOperationWithReflection(functionName string) (err error) {
	functionHolder := validateFunctionHolder{}
	function := reflect.ValueOf(&functionHolder).MethodByName(functionName)
	if !function.IsValid() {
		return fmt.Errorf("Non-existent validate function specified in config file: %s", functionName)
	}
	funcInput := make([]reflect.Value, 1)
	funcInput[0] = reflect.ValueOf(*v.Rows)
	funcOutput := function.Call(funcInput)
	output := funcOutput[0].Interface().([]utils.Row)
	*v.Rows = output
	return nil
}
