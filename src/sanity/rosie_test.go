package sanity

import (
	"encoding/csv"
	"fmt"
	"os"
	"testing"

	"rosie-pattern-language/src/librosie/go/src/rosie"
)

func TestIntegration(t *testing.T) {
	fmt.Println("Here we Go!")
	f, err := os.Open("test.csv")
	if err != nil {
		t.Error(err)
	}
	defer f.Close()
	r := csv.NewReader(f)
	data, err := r.ReadAll()
	if err != nil {
		t.Error(err)
	}
	fmt.Println(data)

	engine, err := rosie.New("My Engine")
	if err != nil {
		t.Error(err)
	}
	cfg, err := engine.Config()
	if err != nil {
		t.Error(err)
	}
	fmt.Println(cfg)

	ok, pkgName, messages, err := engine.ImportPkg("all")
	if err != nil {
		t.Error(err)
	}
	fmt.Println(ok, pkgName, messages)

	pat, messages, err := engine.Compile("all.things")
	if err != nil {
		t.Error(err)
	}
	fmt.Println(pat, messages)

	f2, err := os.Open("test.csv")
	if err != nil {
		t.Error(err)
	}
	defer f2.Close()
	dataBytes := make([]byte, 1000)
	n, err := f2.Read(dataBytes)
	fmt.Println("bytes read", n)
	fmt.Println(dataBytes)
	match, err := pat.Match(dataBytes[:n])
	if err != nil {
		t.Error(err)
	}
	fmt.Println(match.Data)
}
