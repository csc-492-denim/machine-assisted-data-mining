package testutils

import (
	"machine-assisted-data-mining/src/utils"
	"reflect"
)

// InsertRowWithString is a function that insert a Row into []Row with a []string
func InsertRowWithString(rows []utils.Row, insert []string) []utils.Row {
	rows = append(rows, utils.Row{Data: insert, Errors: []error{}, LineNumber: len(rows)})
	return rows
}

// ContainString is a function that checks if a []string in Row.data in []Row
func ContainString(rows []utils.Row, check []string) bool {
	for _, row := range rows {
		if reflect.DeepEqual(row.Data, check) {
			return true
		}
	}
	return false
}

// CheckErrors check for []error for all rows in row.
func CheckErrors(rows []utils.Row) bool {
	for _, row := range rows {
		if len(row.Errors) != 0 {
			return true
		}
	}
	return false
}

// RowDataEqual compares the data of a slice of rows.
// This is useful for tests that don't care about the errors stored
// in the slice of rows.
func RowDataEqual(expected, actual []utils.Row) (equal bool) {
	// If we have a slice out of bounds error or similar, then we just want
	// to return false.
	defer func() {
		if r := recover(); r != nil {
			equal = false
		}
	}()
	if len(expected) != len(actual) {
		return false
	}
	for k := range expected {
		if !reflect.DeepEqual(expected[k].Data, actual[k].Data) {
			return false
		}
	}
	return true
}
