package log

import (
	"errors"
	"fmt"
	"os"
	"strconv"
)

// Action represents a module action
type Action string

// ReadInFileCSV is an Action when reading in a csv file
const ReadInFileCSV = Action("Read in file, filetype: csv")

// ExportFileCSV is an Action when exporting a csv file
const ExportFileCSV = Action("Exporting the data to a files, filetype: CSV")

// Logger object
type Logger struct {
	f *os.File
}

// NewLogger creates a logger that outputs to stderr
func NewLogger() *Logger {
	logger := new(Logger)
	return logger
}

// NewLoggerFile creates a logger that outputs to file
func NewLoggerFile(filename string) *Logger {
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error opening log file %s: %v", filename, err)
		return nil
	}

	logger := new(Logger)
	logger.f = f
	return logger
}

// Destroy Closes files, if any
func (l *Logger) Destroy() {
	if l.f == nil {
		return
	}

	if err := l.f.Close(); err != nil {
		fmt.Fprintf(os.Stderr, "Error destroying logger: %v", err)
	}
}

// LogAction Outputs log entry to file/stderr
func (l *Logger) LogAction(a Action, line int, comment string) error {
	writer := os.Stderr
	if l.f != nil {
		writer = l.f
	}

	_, err := fmt.Fprintf(writer, "Line %6d\t%s\t%s\n", line, a, comment)
	return err
}

// LogAction is a function logging any modification made to the data set
func LogAction(lineNumber int, act interface{}, comment string, err interface{}) error {
	// if not providing an Action
	actMsg, ok := act.(Action)
	if ok == false {
		return errors.New("Please provide a valid Log Action")
	}

	//check err if its a type of error log
	errMsg, ok := err.(errorLog)
	if err != nil && ok == false {
		return errors.New("Please provide a valid Error or nil")
	}
	actMsgString := string(actMsg)
	//logging Action
	actionMsg := "Line: " + strconv.Itoa(lineNumber) + "\t" + actMsgString + "\t" + comment + "\n"
	fmt.Fprintf(os.Stderr, actionMsg)

	errMsgString := string(errMsg)
	// if the error msg is not empty, log the error
	if string(errMsgString) != "" {
		logError(errMsgString)
	}
	return nil
}

type errorLog string

// Int2DoubleError is an error from attempting to convert int to double
const Int2DoubleError = errorLog("Can't convert int to double")

// ExportFileCSVError is an error when exporting to a csv file
const ExportFileCSVError = errorLog("Can't export to csv file")

// LogError is called whenever an error arise in the program
func logError(errMsgString string) {
	errMsg := "\tError: " + errMsgString + "\n"
	fmt.Fprintf(os.Stderr, errMsg)
}
