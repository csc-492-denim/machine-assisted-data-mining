package log

import (
	"io/ioutil"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Testing function for logging
func TestLog(t *testing.T) {
	// no error should be return
	assert.Equal(t, LogAction(-1, ReadInFileCSV, "", nil), nil)
	assert.Equal(t, LogAction(20, ExportFileCSV, "Testing", ExportFileCSVError), nil)
	// error should be returned
	if LogAction(-1, "", "", "") == nil {
		assert.Fail(t, "Failed Expected: error, Actual: No error")
	}
	if LogAction(3, ReadInFileCSV, "", "bad error") == nil {
		assert.Fail(t, "Failed Expected: error, Actual: No error")
	}
	if LogAction(4, 100, "", 22) == nil {
		assert.Fail(t, "Failed Expected: error, Actual: No error")
	}
}

// Test for Logger
func TestLogger(t *testing.T) {
	// test stderr
	l := NewLogger()
	assert.Equal(t, l.LogAction(ReadInFileCSV, 55, "comment"), nil)
	assert.Equal(t, l.LogAction(ExportFileCSV, 24, "comment2"), nil)
	l.Destroy()

	// test file
	testaction := Action("test")
	l2 := NewLoggerFile("_test.log")
	assert.Equal(t, l2.LogAction(testaction, 1241, "hello"), nil)
	l2.Destroy()

	// check the file
	contents, err := ioutil.ReadFile("_test.log")
	if err != nil {
		assert.Fail(t, "error opening _test.log")
	}
	assert.Equal(t, "Line   1241\ttest\thello\n", string(contents))
	os.Remove("_test.log")
}
