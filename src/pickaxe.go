package main

import (
	"flag"
	"fmt"
	"machine-assisted-data-mining/src/controller"
	"os"
)

func main() {
	// This function will receive a file as a command line argument
	// and pass it to the controller, which will be responsible for communicating
	// with the other modules.

	configPtr := flag.String("config", "", "Config file path.")

	flag.Parse()

	fmt.Println("Config Path:", *configPtr)
	cntrl := controller.New(*configPtr)
	err := cntrl.Process()
	if err != nil {
		fmt.Println("Error: ", err)
		os.Exit(1)
	}
}
