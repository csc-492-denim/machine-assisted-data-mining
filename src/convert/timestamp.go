package convert

import (
	"fmt"
	"machine-assisted-data-mining/src/utils"
	"strconv"
	"strings"
	"time"
)

var months = map[string]int{
	"January":   1,
	"February":  2,
	"March":     3,
	"April":     4,
	"May":       5,
	"June":      6,
	"July":      7,
	"August":    8,
	"September": 9,
	"October":   10,
	"November":  11,
	"December":  12,
	"Jan":       1,
	"Feb":       2,
	"Mar":       3,
	"Apr":       4,
	"Jun":       6,
	"Jul":       7,
	"Aug":       8,
	"Sep":       9,
	"Oct":       10,
	"Nov":       11,
	"Dec":       12,
}

// getZone returns the zone and the offset for the minutes or hours
func getZone(data string) (loc *time.Location, hoffset int, moffset int, err error) {
	// This expects the format for the input as a word representing the timezone
	// (example: `EST`)
	// or 5 characters containing +/-4DIGITS (example: `-0700`)
	// or 6 characters containing +/-2DIGITS:2DIGITS (example: `+03:50`)
	// or the character `z`, meaning it's in UTC

	loc = time.UTC
	if strings.EqualFold(data, "z") { // This is UTC time
		return
	}
	end := len(data)
	hoffset = 0
	moffset = 0
	if data[0] == '-' {
		hoffset, err = strconv.Atoi(data[1:3])
		moffset, err = strconv.Atoi(data[end-2 : end])
		return
	}
	if data[0] == '+' {
		hoffset, err = strconv.Atoi(data[1:3])
		hoffset *= -1
		moffset, err = strconv.Atoi(data[end-2 : end])
		moffset *= -1
		return
	}
	loc, err = time.LoadLocation(data)
	return
}

// getMonth gets the month by checking first if the month is in the table of month words, then converting to an int
func getMonth(data string) (month int, err error) {
	month = months[data]
	if month == 0 {
		month, err = strconv.Atoi(data)
	}
	return
}

func getYear(data string) (year int, err error) {
	year, err = strconv.Atoi(data)
	if err != nil {
		return
	}
	if year < 100 { // If it's a 2 digit year, make it 4 digit
		currentYear := time.Now().Year()
		current2DigitYear := currentYear % 1000
		currentCent := currentYear / 100
		if year <= current2DigitYear {
			year = year + currentCent*100
		} else {
			year = year + (currentCent-1)*100
		}
	}
	return
}

// toTime converts into a go date type
func toTime(matchData utils.MatchData) (date time.Time, err error) {
	monthMD := matchData.SearchContains("date.month")
	dayMD := matchData.Search("date.day")
	yearMD := matchData.SearchMany("date.short_long_year", "date.year")
	hourMD := matchData.SearchMany("time.hour", "time.short_hour")
	minuteMD := matchData.Search("time.minute")
	secondMD := matchData.Search("time.second")
	nsecMD := matchData.Search("time.secfrac")
	zoneMD := matchData.SearchMany("time.offset", "time.rfc2822_zone", "time.rcf3339_zone")
	ampmMD := matchData.Search("time.ampm")

	var day, year, month int
	hour := 0
	min := 0
	sec := 0
	nsec := 0
	loc := time.UTC

	if len(monthMD) < 0 {
		err = fmt.Errorf("Can't find month")
		return
	}
	month, err = getMonth(monthMD[0].Data)
	if err != nil {
		return
	}
	if len(dayMD) < 0 {
		err = fmt.Errorf("Can't find Day")
		return
	}
	day, err = strconv.Atoi(dayMD[0].Data)
	if err != nil {
		return
	}

	if len(yearMD) < 0 {
		err = fmt.Errorf("Can't find Year")
		return
	}
	year, err = getYear(yearMD[0].Data)
	if err != nil {
		return
	}

	if len(hourMD) > 0 {
		hour, err = strconv.Atoi(hourMD[0].Data)
		if err != nil {
			return
		}
		if len(ampmMD) > 0 && strings.EqualFold(ampmMD[0].Data, "pm") {
			hour += 12
		}
	}
	if len(minuteMD) > 0 {
		min, err = strconv.Atoi(minuteMD[0].Data)
		if err != nil {
			return
		}
	}
	if len(secondMD) > 0 {
		sec, err = strconv.Atoi(secondMD[0].Data)
		if err != nil {
			return
		}
	}
	if len(nsecMD) > 0 {
		nsec, err = getFracSec(nsecMD[0].Data)
		if err != nil {
			return
		}
	}
	if len(zoneMD) > 0 {
		hoffset := 0
		moffset := 0
		loc, hoffset, moffset, err = getZone(zoneMD[0].Data)

		if err != nil {
			return
		}
		min = min + moffset
		hour = hour + hoffset
	}
	date = time.Date(year, time.Month(month), day, hour, min, sec, nsec, loc)
	return date.UTC(), err
}

// getFracSec returns the nanoseconds as an integer
func getFracSec(data string) (int, error) {
	tempNSec := 0.0
	if data[0] == ',' { // Some time formats use a comma
		data = "." + data[1:len(data)]
	}
	tempNSec, err := strconv.ParseFloat(data, 32)
	if err != nil {
		return 0, err
	}
	nsec := int(tempNSec * 100000000) // date() wants nanoseconds
	return nsec, nil
}

// ToSpaced converts to a spaced timestamp YYYY MM DD
func (c conversionFunctionHolder) ToSpaced(matchData utils.MatchData) (string, error) {

	date, err := toTime(matchData)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%d %d %d", date.Year(), date.Month(), date.Day()), nil
}

// ToDashed YYYY-MM-DD
func (c conversionFunctionHolder) ToDashed(matchData utils.MatchData) (string, error) {

	date, err := toTime(matchData)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%d-%d-%d", date.Year(), date.Month(), date.Day()), nil
}

// ToSlashed YYYY/MM/DD
func (c conversionFunctionHolder) ToSlashed(matchData utils.MatchData) (string, error) {

	date, err := toTime(matchData)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%d/%d/%d", date.Year(), date.Month(), date.Day()), nil
}

// ToSpacedEn YYYY month_name DD
func (c conversionFunctionHolder) ToSpacedEn(matchData utils.MatchData) (string, error) {

	date, err := toTime(matchData)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%d %s %d", date.Year(), date.Month(), date.Day()), nil
}

// ToUSDashed MM-DD-YYYY
func (c conversionFunctionHolder) ToUSDashed(matchData utils.MatchData) (string, error) {

	date, err := toTime(matchData)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%d-%d-%d", date.Month(), date.Day(), date.Year()), nil
}

// ToUSSlashed MM/DD/YYYY
func (c conversionFunctionHolder) ToUSSlashed(matchData utils.MatchData) (string, error) {

	date, err := toTime(matchData)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%d/%d/%d", date.Month(), date.Day(), date.Year()), nil
}

// ToEur DD.MM.YYYY
func (c conversionFunctionHolder) ToEur(matchData utils.MatchData) (string, error) {

	date, err := toTime(matchData)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%d.%d.%d", date.Day(), date.Month(), date.Year()), nil
}

// ToUSLong month_name DD YYYY
func (c conversionFunctionHolder) ToUSLong(matchData utils.MatchData) (string, error) {

	date, err := toTime(matchData)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s %d %d", date.Month(), date.Day(), date.Year()), nil
}

// ToStandardTime converts to a dashed timestamp in UTC
func (c conversionFunctionHolder) ToDashedUTC(matchData utils.MatchData) (string, error) {
	date, err := toTime(matchData)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%d-%d-%d %02d:%02d:%02d.%02d %s", date.Year(), date.Month(), date.Day(), date.Hour(), date.Minute(), date.Second(), date.Nanosecond()/1000000, date.Location()), nil
}

// ToSlashedTime converts to a slashed timestamp in UTC
func (c conversionFunctionHolder) ToSlashedUTC(matchData utils.MatchData) (string, error) {
	date, err := toTime(matchData)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%d/%d/%d %02d:%02d:%02d.%02d %s", date.Year(), date.Month(), date.Day(), date.Hour(), date.Minute(), date.Second(), date.Nanosecond()/1000000, date.Location()), nil
}

// ToUSLongTime converts to a spaced timestamp with month name in UTC
func (c conversionFunctionHolder) ToUSLongUTC(matchData utils.MatchData) (string, error) {
	date, err := toTime(matchData)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s %d %d %02d:%02d:%02d.%02d %s", date.Month(), date.Day(), date.Year(), date.Hour(), date.Minute(), date.Second(), date.Nanosecond()/1000000, date.Location()), nil
}

// ToEST converts to a dashed timestamp in EST
func (c conversionFunctionHolder) ToDashedEST(matchData utils.MatchData) (string, error) {
	date, err := toTime(matchData)
	if err != nil {
		return "", err
	}
	est, _ := time.LoadLocation("EST")
	date = date.In(est)
	return fmt.Sprintf("%d-%d-%d %02d:%02d:%02d.%02d %s", date.Year(), date.Month(), date.Day(), date.Hour(), date.Minute(), date.Second(), date.Nanosecond()/1000000, date.Location()), nil
}
