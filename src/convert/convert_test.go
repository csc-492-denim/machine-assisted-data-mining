package convert

import (
	"errors"
	"machine-assisted-data-mining/src/testutils"
	"machine-assisted-data-mining/src/utils"
	"reflect"
	"strings"
	"testing"
)

func runColumnConvertTest(t *testing.T, input, expectedOutput []utils.Row, typeToMatch string, functionName string, column int, recordErrors bool) {
	converter := New(input)
	err := converter.Convert(typeToMatch, functionName, column, recordErrors)
	result := converter.rows
	if err != nil {
		t.Error(err)
	}
	if !reflect.DeepEqual(result, expectedOutput) {
		t.Errorf("Conversion was incorrect.  Expected %v, but got %v", expectedOutput, result)
	}
}

// TestIntToFloat verifies that a column of ints is correctly converted
// to a column of floats.
func TestIntToFloat(t *testing.T) {
	input := []utils.Row{}
	input = testutils.InsertRowWithString(input, []string{"2", "5", "3"})
	input = testutils.InsertRowWithString(input, []string{"7", "3", "1"})

	expected := []utils.Row{}
	expected = testutils.InsertRowWithString(expected, []string{"2", "5.0", "3"})
	expected = testutils.InsertRowWithString(expected, []string{"7", "3.0", "1"})

	runColumnConvertTest(t, input, expected, "num.int", "IntToFloat", 1, true)
}

func TestMixedIntToFloat(t *testing.T) {
	input := []utils.Row{}
	input = testutils.InsertRowWithString(input, []string{"2 string", "5", "3.0"})
	input = testutils.InsertRowWithString(input, []string{"string 5", "3", "1"})
	input = testutils.InsertRowWithString(input, []string{"3.0", "3", "1"})
	input = testutils.InsertRowWithString(input, []string{"7", "3", "1"})

	expected := []utils.Row{}
	expected = testutils.InsertRowWithString(expected, []string{"2 string", "5", "3.0"})
	expected[0].AddError(errors.New("Type did not match in column: 0"))
	expected = testutils.InsertRowWithString(expected, []string{"string 5", "3", "1"})
	expected[1].AddError(errors.New("Type did not match in column: 0"))
	expected = testutils.InsertRowWithString(expected, []string{"3.0", "3", "1"})
	expected[2].AddError(errors.New("Type did not match in column: 0"))
	expected = testutils.InsertRowWithString(expected, []string{"7.0", "3", "1"})

	runColumnConvertTest(t, input, expected, "num.int", "IntToFloat", 0, true)
}

func TestColumnIndexOutOfRange(t *testing.T) {
	input := []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3"},
			Errors: []error{},
		},
	}
	expected := []utils.Row{
		utils.Row{
			Data:   []string{"1", "2.0"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3"},
			Errors: []error{errors.New("Column: index out of range")},
		},
	}
	runColumnConvertTest(t, input, expected, "num.int", "IntToFloat", 1, true)
}

func TestBadConversionFunction(t *testing.T) {
	input := []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3"},
			Errors: []error{},
		},
	}
	converter := New(input)
	err := converter.Convert("num.int", "Panda", 0, true)
	if err == nil {
		t.Errorf("Expected non-nil error")
	}
	if !strings.Contains(err.Error(), "Non-existent conversion function") {
		t.Errorf("Error didn't contain expected value.  Actual: %s", err)
	}
}

func TestConvertAll(t *testing.T) {
	input := []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3", "4"},
			Errors: []error{},
		},
	}
	expected := []utils.Row{
		utils.Row{
			Data:   []string{"1.0", "2.0"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3.0", "4.0"},
			Errors: []error{},
		},
	}
	converter := New(input)
	err := converter.ConvertAll("num.int", "IntToFloat", true)
	if err != nil {
		t.Errorf("Unexpected error: %s", err)
	}
	if !reflect.DeepEqual(converter.rows, expected) {
		t.Errorf("Conversion was incorrect.  Expected %v, but got %v", expected, converter.rows)
	}
}

func TestTimestampToSpaced(t *testing.T) {
	input := []utils.Row{}
	input = testutils.InsertRowWithString(input, []string{"12/8/2017", "3", "m"})
	input = testutils.InsertRowWithString(input, []string{"March 19 2018", "7", "j"})
	input = testutils.InsertRowWithString(input, []string{"Nov 1 2014", "100", "st"})
	input = testutils.InsertRowWithString(input, []string{"8-15-1996", "12", "h"})
	input = testutils.InsertRowWithString(input, []string{"19.5.2002", "12", "hs"})
	input = testutils.InsertRowWithString(input, []string{"2003/10/1", "24", "tl"})
	input = testutils.InsertRowWithString(input, []string{"2015-04-05", "3", "e"})
	input = testutils.InsertRowWithString(input, []string{"2012 December 12", "31", "jd"})

	expected := []utils.Row{}
	expected = testutils.InsertRowWithString(expected, []string{"2017 12 8", "3", "m"}) //year month day
	expected = testutils.InsertRowWithString(expected, []string{"2018 3 19", "7", "j"})
	expected = testutils.InsertRowWithString(expected, []string{"2014 11 1", "100", "st"})
	expected = testutils.InsertRowWithString(expected, []string{"1996 8 15", "12", "h"})
	expected = testutils.InsertRowWithString(expected, []string{"2002 5 19", "12", "hs"})
	expected = testutils.InsertRowWithString(expected, []string{"2003 10 1", "24", "tl"})
	expected = testutils.InsertRowWithString(expected, []string{"2015 4 5", "3", "e"})
	expected = testutils.InsertRowWithString(expected, []string{"2012 12 12", "31", "jd"})

	runColumnConvertTest(t, input, expected, "date.any", "ToSpaced", 0, true)
}

func TestTimestampToDashed(t *testing.T) {
	input := []utils.Row{}
	input = testutils.InsertRowWithString(input, []string{"12/8/2017", "3", "m"})
	input = testutils.InsertRowWithString(input, []string{"March 19 2018", "7", "j"})

	expected := []utils.Row{}
	expected = testutils.InsertRowWithString(expected, []string{"2017-12-8", "3", "m"}) //year month day
	expected = testutils.InsertRowWithString(expected, []string{"2018-3-19", "7", "j"})

	runColumnConvertTest(t, input, expected, "date.any", "ToDashed", 0, true)
}

func TestTimestampToSlashed(t *testing.T) {
	input := []utils.Row{}
	input = testutils.InsertRowWithString(input, []string{"12/8/2017", "3", "m"})
	input = testutils.InsertRowWithString(input, []string{"March 19 2018", "7", "j"})
	input = testutils.InsertRowWithString(input, []string{"2015-04-05", "3", "e"})

	expected := []utils.Row{}
	expected = testutils.InsertRowWithString(expected, []string{"2017/12/8", "3", "m"}) //year month day
	expected = testutils.InsertRowWithString(expected, []string{"2018/3/19", "7", "j"})
	expected = testutils.InsertRowWithString(expected, []string{"2015/4/5", "3", "e"})

	runColumnConvertTest(t, input, expected, "date.any", "ToSlashed", 0, true)
}

func TestTimestampToSpacedEn(t *testing.T) {
	input := []utils.Row{}
	input = testutils.InsertRowWithString(input, []string{"12/8/2017", "3", "m"})
	input = testutils.InsertRowWithString(input, []string{"March 19 2018", "7", "j"})
	input = testutils.InsertRowWithString(input, []string{"2015-04-05", "3", "e"})

	expected := []utils.Row{}
	expected = testutils.InsertRowWithString(expected, []string{"2017 December 8", "3", "m"}) //year month day
	expected = testutils.InsertRowWithString(expected, []string{"2018 March 19", "7", "j"})
	expected = testutils.InsertRowWithString(expected, []string{"2015 April 5", "3", "e"})

	runColumnConvertTest(t, input, expected, "date.any", "ToSpacedEn", 0, true)
}

func TestTimestampToUSDashed(t *testing.T) {
	input := []utils.Row{}
	input = testutils.InsertRowWithString(input, []string{"12/8/2017", "3", "m"})
	input = testutils.InsertRowWithString(input, []string{"March 19 2018", "7", "j"})
	input = testutils.InsertRowWithString(input, []string{"2015-04-05", "3", "e"})

	expected := []utils.Row{}
	expected = testutils.InsertRowWithString(expected, []string{"12-8-2017", "3", "m"}) //year month day
	expected = testutils.InsertRowWithString(expected, []string{"3-19-2018", "7", "j"})
	expected = testutils.InsertRowWithString(expected, []string{"4-5-2015", "3", "e"})

	runColumnConvertTest(t, input, expected, "date.any", "ToUSDashed", 0, true)
}

func TestTimestampToUSSlashed(t *testing.T) {
	input := []utils.Row{}
	input = testutils.InsertRowWithString(input, []string{"March 19 2018", "7", "j"})
	input = testutils.InsertRowWithString(input, []string{"2015-04-05", "3", "e"})

	expected := []utils.Row{}
	expected = testutils.InsertRowWithString(expected, []string{"3/19/2018", "7", "j"})
	expected = testutils.InsertRowWithString(expected, []string{"4/5/2015", "3", "e"})

	runColumnConvertTest(t, input, expected, "date.any", "ToUSSlashed", 0, true)
}

func TestTimestampToEur(t *testing.T) {
	input := []utils.Row{}
	input = testutils.InsertRowWithString(input, []string{"12/8/2017", "3", "m"})
	input = testutils.InsertRowWithString(input, []string{"March 19 2018", "7", "j"})
	input = testutils.InsertRowWithString(input, []string{"2015-04-05", "3", "e"})

	expected := []utils.Row{}
	expected = testutils.InsertRowWithString(expected, []string{"8.12.2017", "3", "m"}) //year month day
	expected = testutils.InsertRowWithString(expected, []string{"19.3.2018", "7", "j"})
	expected = testutils.InsertRowWithString(expected, []string{"5.4.2015", "3", "e"})

	runColumnConvertTest(t, input, expected, "date.any", "ToEur", 0, true)
}

func TestTimestampToUSLong(t *testing.T) {
	input := []utils.Row{}
	input = testutils.InsertRowWithString(input, []string{"12/8/2017", "3", "m"})
	input = testutils.InsertRowWithString(input, []string{"2015-04-05", "3", "e"})

	expected := []utils.Row{}
	expected = testutils.InsertRowWithString(expected, []string{"December 8 2017", "3", "m"})
	expected = testutils.InsertRowWithString(expected, []string{"April 5 2015", "3", "e"})

	runColumnConvertTest(t, input, expected, "date.any", "ToUSLong", 0, true)
}

func TestDontRecordErrors(t *testing.T) {
	input := []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3.0", "4"},
			Errors: []error{},
		},
	}
	expected := []utils.Row{
		utils.Row{
			Data:   []string{"1.0", "2.0"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3.0", "4.0"},
			Errors: []error{},
		},
	}
	converter := New(input)
	err := converter.ConvertAll("num.int", "IntToFloat", false)
	if err != nil {
		t.Errorf("Unexpected error: %s", err)
	}
	if !reflect.DeepEqual(converter.rows, expected) {
		t.Errorf("Conversion was incorrect.  Expected %v, but got %v", expected, converter.rows)
	}
}

func TestStandardTimestamp(t *testing.T) {
	input := []utils.Row{}
	input = testutils.InsertRowWithString(input, []string{"2000 Dec 8 13:16:49 EST", "3", "m"})
	input = testutils.InsertRowWithString(input, []string{"4/5/2015 4:03:15,5 PM", "3", "e"})
	input = testutils.InsertRowWithString(input, []string{"Mar 19 2018 1:03:15.25 AM", "7", "j"})
	input = testutils.InsertRowWithString(input, []string{"12-8-15 9:15:17", "3", "m"})
	input = testutils.InsertRowWithString(input, []string{"8-15-96 7:15:16.2", "12", "h"})
	input = testutils.InsertRowWithString(input, []string{"8-15-96 7:15:16.2 -0700", "12", "h"})
	input = testutils.InsertRowWithString(input, []string{"1996-8-15 7:15:16+0330", "12", "h"})
	input = testutils.InsertRowWithString(input, []string{"1996-08-15T07:15:16+03:30", "12", "h"})
	input = testutils.InsertRowWithString(input, []string{"1996-08-15T07:15:16.2Z", "12", "h"})

	expected := []utils.Row{}
	expected = testutils.InsertRowWithString(expected, []string{"2000-12-8 18:16:49.00 UTC", "3", "m"})
	expected = testutils.InsertRowWithString(expected, []string{"2015-4-5 16:03:15.50 UTC", "3", "e"})
	expected = testutils.InsertRowWithString(expected, []string{"2018-3-19 01:03:15.25 UTC", "7", "j"})
	expected = testutils.InsertRowWithString(expected, []string{"2015-12-8 09:15:17.00 UTC", "3", "m"})
	expected = testutils.InsertRowWithString(expected, []string{"1996-8-15 07:15:16.20 UTC", "12", "h"})
	expected = testutils.InsertRowWithString(expected, []string{"1996-8-15 14:15:16.20 UTC", "12", "h"})
	expected = testutils.InsertRowWithString(expected, []string{"1996-8-15 03:45:16.00 UTC", "12", "h"})
	expected = testutils.InsertRowWithString(expected, []string{"1996-8-15 03:45:16.00 UTC", "12", "h"})
	expected = testutils.InsertRowWithString(expected, []string{"1996-8-15 07:15:16.20 UTC", "12", "h"})

	runColumnConvertTest(t, input, expected, "ts.any", "ToDashedUTC", 0, true)
}

func TestSlashedTimestamp(t *testing.T) {
	input := []utils.Row{}
	input = testutils.InsertRowWithString(input, []string{"2000 Dec 8 13:16:49 EST", "3", "m"})
	input = testutils.InsertRowWithString(input, []string{"4-5-2015 4:03:15,5 PM", "3", "e"})

	expected := []utils.Row{}
	expected = testutils.InsertRowWithString(expected, []string{"2000/12/8 18:16:49.00 UTC", "3", "m"})
	expected = testutils.InsertRowWithString(expected, []string{"2015/4/5 16:03:15.50 UTC", "3", "e"})

	runColumnConvertTest(t, input, expected, "ts.any", "ToSlashedUTC", 0, true)
}

func TestUSLongTimestamp(t *testing.T) {
	input := []utils.Row{}
	input = testutils.InsertRowWithString(input, []string{"2000 Dec 8 13:16:49 EST", "3", "m"})
	input = testutils.InsertRowWithString(input, []string{"4-5-2015 4:03:15,5 PM", "3", "e"})

	expected := []utils.Row{}
	expected = testutils.InsertRowWithString(expected, []string{"December 8 2000 18:16:49.00 UTC", "3", "m"})
	expected = testutils.InsertRowWithString(expected, []string{"April 5 2015 16:03:15.50 UTC", "3", "e"})

	runColumnConvertTest(t, input, expected, "ts.any", "ToUSLongUTC", 0, true)
}

func TestEstTimestamp(t *testing.T) {
	input := []utils.Row{}
	input = testutils.InsertRowWithString(input, []string{"2000 Dec 8 13:16:49 EST", "3", "m"})
	input = testutils.InsertRowWithString(input, []string{"4/5/2015 4:03:15,5 PM", "3", "e"})

	expected := []utils.Row{}
	expected = testutils.InsertRowWithString(expected, []string{"2000-12-8 13:16:49.00 EST", "3", "m"})
	expected = testutils.InsertRowWithString(expected, []string{"2015-4-5 11:03:15.50 EST", "3", "e"})

	runColumnConvertTest(t, input, expected, "ts.any", "ToDashedEST", 0, true)
}
