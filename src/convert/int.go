package convert

import (
	"fmt"
	"machine-assisted-data-mining/src/utils"
)

// IntToFloat converts a string of integer-formatted data to float-formatted.
// Eg 3 -> 3.0
func (c conversionFunctionHolder) IntToFloat(matchData utils.MatchData) (output string, err error) {
	numb := matchData.Search("num.int")
	if len(numb) > 0 {
		numb[0].Data = numb[0].Data + ".0"
		output = numb[0].Data
		return output, nil
	}
	err = fmt.Errorf("Error converting int to float. Incorrect format")
	return "", err
}
