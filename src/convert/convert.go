package convert

import (
	"errors"
	"fmt"
	"machine-assisted-data-mining/src/utils"
	"reflect"
	"rosie-pattern-language/src/librosie/go/src/rosie"
)

// New converter using a Row
func New(input []utils.Row) Converter {
	return Converter{rows: input}
}

// Converter has rows
type Converter struct {
	rows []utils.Row
}

// matchRosie returns a matchData for the specified pattern on the input
func matchRosie(input string, pat *rosie.Pattern) (output utils.MatchData, err error) {
	match, err := pat.MatchString(input)
	if err != nil {
		return
	}
	matchData := *utils.CreateMatchData(match)
	return matchData, nil
}

// checkType checks that the type is found in the match
func checkType(matchData utils.MatchData, typeToMatch string) (output bool) {
	for matchData.Subs != nil {
		if hasMultipleSubs(matchData) {
			break
		}
		matchData = matchData.Subs[0]
		if matchData.Type == typeToMatch {
			return true
		}
	}
	return false
}

// If a map has multiple subs, then that means rosie has started
// breaking up the field into multiple types.
// For checkType, we want to match the most specific type that applies
// to the entire field, so we don't want to traverse the tree of types further
// if there are multiple subs.
func hasMultipleSubs(m utils.MatchData) bool {
	return len(m.Subs) > 1
}

// Convert converts from the given type by the given operation
func (converter Converter) Convert(typeToMatch string, functionName string, column int, recordFormatErrors bool) (err error) {
	r, err := utils.NewRosieEngine("all")
	if err != nil {
		return err
	}
	pat, _, err := r.Compile("all.things")
	if err != nil {
		return fmt.Errorf("Can not load in all things pattern in Rosie: %s", err)
	}
	for i := 0; i < len(converter.rows); i++ {
		row := &converter.rows[i]
		if row.Deleted {
			continue
		}
		if len(row.Data) <= column { // If the row does not have the specfied column
			rowErr := errors.New("Column: index out of range")
			row.AddError(rowErr)
			continue
		}
		matchData, err := matchRosie(row.Data[column], pat)
		if err != nil {
			row.AddError(err)
			continue
		}
		if !checkType(matchData, typeToMatch) {
			if recordFormatErrors {
				row.AddError(fmt.Errorf("Type did not match in column: %d", column))
			}
			continue
		}
		newData, err := callOperationWithReflection(matchData, functionName)
		if err != nil {
			// At this point, if we get an error, then we're doing something wrong that we should fail for.
			// This could be calling a non-existent function, or passing an un-supported data type into a convert
			// function (not just a specific field being different - that would be caught by checkType)
			return err
		}
		converter.rows[i].Data[column] = newData
	}
	return
}

// ConvertAll calls the specified conversion operation on all columns.
func (converter Converter) ConvertAll(typeToMatch string, functionName string, recordFormatErrors bool) (err error) {
	if len(converter.rows) == 0 {
		return nil
	}
	for col := 0; col < len(converter.rows[0].Data); col++ {
		err = converter.Convert(typeToMatch, functionName, col, recordFormatErrors)
		if err != nil {
			return err
		}
	}
	return nil
}

func callOperationWithReflection(input utils.MatchData, functionName string) (output string, err error) {
	converter := conversionFunctionHolder{}
	function := reflect.ValueOf(&converter).MethodByName(functionName)
	if !function.IsValid() {
		return "", fmt.Errorf("Non-existent conversion function specified in config file: %s", functionName)
	}
	funcInput := make([]reflect.Value, 1)
	funcInput[0] = reflect.ValueOf(input)
	funcOutput := function.Call(funcInput)
	output = funcOutput[0].Interface().(string)
	err, _ = funcOutput[1].Interface().(error)
	return output, err
}

type conversionFunctionHolder struct{}
