package controller

import (
	"fmt"
	"machine-assisted-data-mining/src/config"
	"machine-assisted-data-mining/src/convert"
	"machine-assisted-data-mining/src/export"
	"machine-assisted-data-mining/src/load"
	"machine-assisted-data-mining/src/restructure"
	"machine-assisted-data-mining/src/utils"
	"machine-assisted-data-mining/src/validate"
	"reflect"
	"runtime"
	"sort"
	"strconv"
)

// Controller has a config and a set of data to process
type Controller struct {
	cfgPath string
	cfg     config.Config
	data    []utils.Row
	header  utils.Row
}

// New returns a new controller
func New(cfgPath string) Controller {
	return Controller{cfgPath: cfgPath}
}

// Process processes a data file as indicated in the given config file
func (c *Controller) Process() error {
	err := c.loadConfig()
	if err != nil {
		return err
	}
	for _, task := range c.cfg.Tasks {
		operation := task.Operation
		// blacklist of unsupported operations.  Allowing Process would break since it takes no arguments.
		if operation == "Process" {
			return fmt.Errorf("Unsupported operation: %s", operation)
		}
		args := task.Args
		err = c.callOperationWithReflection(operation, args)
		if err != nil {
			return err
		}
	}
	return nil
}

// callOperationWithReflection takes a function name and a map of arguments and looks up that function and calls
// it with the given arguments.
func (c *Controller) callOperationWithReflection(operation string, args map[string]interface{}) (err error) {
	function := reflect.ValueOf(c).MethodByName(operation)
	if !function.IsValid() {
		return fmt.Errorf("Non-existent operation specified in config file: %s", operation)
	}
	funcInput := make([]reflect.Value, 1)
	funcInput[0] = reflect.ValueOf(args)
	funcOutput := function.Call(funcInput)
	outputError := funcOutput[0].Interface()
	if outputError != nil {
		return outputError.(error)
	}
	return nil
}

// LoadConfig loads a config file and stores it in the controller's config
func (c *Controller) loadConfig() error {
	cfg, err := config.Load(c.cfgPath)
	if err != nil {
		return err
	}
	c.cfg = cfg
	return nil
}

// Load loads the data specified by the args into the controller
func (c *Controller) Load(args map[string]interface{}) error {
	startInt := 0
	endInt := -99

	start, ok := args["start"]
	if ok {
		startInt, ok = start.(int)
		if !ok {
			return fmt.Errorf("Start specified, but not int for load operation")
		}
	}

	end, ok := args["end"]
	if ok {
		endInt, ok = end.(int)
		if !ok {
			return fmt.Errorf("End specified, but not int for load operation")
		}
	}

	path, ok := args["path"]
	if !ok {
		return fmt.Errorf("Path not specified for load operation")
	}
	pathStr, ok := path.(string)
	if !ok {
		return fmt.Errorf("Path specified, but not string for load operation")
	}

	// This argument is optional, defaults to false
	headerPresent, ok := args["header_present"]
	headerBool := false
	if ok {
		headerBool, ok = headerPresent.(bool)
		if !ok {
			return fmt.Errorf("header_present specified, but not boolean for load operation")
		}
	}

	loader := load.New(pathStr)

	if headerBool {
		header, err := loader.LoadStartEnd(0, 0)
		if err != nil {
			return err
		}
		c.header = header[0]
		if startInt == 0 {
			startInt++
		}
	}

	data, err := loader.LoadStartEnd(startInt, endInt)

	if err != nil {
		return err
	}
	c.data = data
	return nil
}

// Convert runs the conversion specified by args
func (c *Controller) Convert(args map[string]interface{}) error {
	typeToMatch, ok := args["type"]
	if !ok {
		return fmt.Errorf("Type not specified for convert operation")
	}
	typeStr, ok := typeToMatch.(string)
	if !ok {
		return fmt.Errorf("Type specified, but not string for convert operation")
	}
	// This argument is optional, defaults to true
	recordErrors, ok := args["record_format_errors"]
	recordBool := true
	if ok {
		recordBool, ok = recordErrors.(bool)
		if !ok {
			return fmt.Errorf("record_format_errors specified, but not boolean for convert operation")
		}
	}
	function, ok := args["function"]
	if !ok {
		return fmt.Errorf("Function not specified for convert operation")
	}
	functionStr, ok := function.(string)
	if !ok {
		return fmt.Errorf("Function specified, but not string for convert operation")
	}
	column, ok := args["column"]
	if !ok {
		return fmt.Errorf("Column not specified for convert operation")
	}
	columnStr, ok := column.(string)
	if !ok {
		return fmt.Errorf("Column specified, but not string for convert operation")
	}

	length, numThreads, dataChan, errChan, threadNumChan, chunkSize, err := c.getThreadRequirements()
	if err != nil {
		return err
	}

	for i := 0; i < numThreads; i++ {
		size := getAmountOfDataPerThread(i, length, numThreads, chunkSize)
		data := make([]utils.Row, size)

		copy(data, c.data[i*chunkSize:])
		go convertThread(data, columnStr, typeStr, functionStr, recordBool, dataChan, errChan, threadNumChan, i)
	}

	err = getErrors(errChan, numThreads)
	if err != nil {
		return err
	}

	convertedData := getAndRecombineData(numThreads, threadNumChan, dataChan)

	c.data = convertedData
	return nil
}

// convertThread handles the atomic operations that each thread runs on the data.
// Each thread calls into the convert package and then passes its return values back over a set of channels.
func convertThread(data []utils.Row, columnStr string, typeStr string, functionStr string, recordErrors bool, dataChan chan []utils.Row, errChan chan error, threadNumChan chan int, threadNum int) {

	converter := convert.New(data)
	if columnStr == "all" {
		err := converter.ConvertAll(typeStr, functionStr, recordErrors)
		errChan <- err
		threadNumChan <- threadNum
		dataChan <- data
		return
	}
	columnIndex, err := strconv.Atoi(columnStr)
	if err != nil {
		err := fmt.Errorf("Unable to convert column index to int: %s", err)
		errChan <- err
		threadNumChan <- threadNum
		dataChan <- data
		return
	}
	err = converter.Convert(typeStr, functionStr, columnIndex, recordErrors)
	errChan <- err
	threadNumChan <- threadNum
	dataChan <- data
}

// getThreadRequirements uses the length of the data stored in the controller and the
// number of cores available on the machine to create several variables and channels
// needed to process data on multiple threads.
func (c *Controller) getThreadRequirements() (length int, numThreads int, dataChan chan []utils.Row, errChan chan error, threadNumChan chan int, chunkSize int, err error) {
	length = len(c.data)
	if length == 0 {
		return -1, -1, nil, nil, nil, -1, fmt.Errorf("No data to process")
	}
	numThreads = getNumThreads(length)
	dataChan = make(chan []utils.Row, numThreads)
	errChan = make(chan error, numThreads)
	threadNumChan = make(chan int, numThreads)
	chunkSize = length / numThreads
	return
}

// getAmountOfDataPerThread determines how much of the total data set each thread will process.
func getAmountOfDataPerThread(i, dataLength, numThreads, chunkSize int) int {
	if i+1 == numThreads {
		return dataLength - i*chunkSize
	}
	return chunkSize
}

// getNumThreads determines how many threads can be used to process the data.
func getNumThreads(dataLength int) int {
	numCPU := runtime.NumCPU()
	if dataLength < numCPU {
		return dataLength
	}
	return numCPU
}

// getErrors reads all errors out of the error channel, returning early if one is found.  These errors would be
// fatal, not processing errors (which would be stored in the error stack of each row).
func getErrors(errChan chan error, numThreads int) error {
	for i := 0; i < numThreads; i++ {
		err := <-errChan
		if err != nil {
			return err
		}
	}
	return nil
}

// getAndRecombineData reads all of the processed data out of the data channel and uses the corresponding
// thread number on the threadNum channel to put it back together in the correct order.
func getAndRecombineData(numThreads int, threadNumChan chan int, dataChan chan []utils.Row) []utils.Row {
	dataMap := make(map[int][]utils.Row)
	for i := 0; i < numThreads; i++ {
		threadNum := <-threadNumChan
		data := <-dataChan
		dataMap[threadNum] = data
	}

	data := make([]utils.Row, 0)
	keys := make([]int, 0)
	for k := range dataMap {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	for k := range keys {
		data = append(data, dataMap[k]...)
	}
	return data
}

// Restructure restructures a column into one or more new columns
func (c *Controller) Restructure(args map[string]interface{}) error {
	pattern, ok := args["pattern"]
	if !ok {
		return fmt.Errorf("Pattern not specified for restructure operation")
	}
	patternStr, ok := pattern.(string)
	if !ok {
		return fmt.Errorf("Pattern specified, but not string for restructure operation")
	}

	variables, ok := args["variables"]
	if !ok {
		return fmt.Errorf("Variables not specified for restructure operation")
	}
	variablesInterfaceMap, ok := variables.(map[interface{}]interface{})
	if !ok {
		return fmt.Errorf("Variables specified, but not map for restructure operation")
	}

	variablesMap, err := utils.ConvertToStringMap(variablesInterfaceMap)
	if err != nil {
		return fmt.Errorf("Error occured when processing variables in Restructure: %s", err)
	}

	dependencies, ok := args["dependencies"]
	if !ok {
		return fmt.Errorf("Dependencies not specified for restructure operation")
	}
	dependenciesInterfaceList, ok := dependencies.([]interface{})
	if !ok {
		return fmt.Errorf("Dependencies specified, but not list for restructure operation")
	}

	dependenciesList, err := utils.ConvertToStringList(dependenciesInterfaceList)
	if err != nil {
		return fmt.Errorf("Error occured when processing dependencies in Restructure: %s", err)
	}

	column, ok := args["column"]
	if !ok {
		return fmt.Errorf("Column not specified for restructure operation")
	}
	columnStr, ok := column.(string)
	if !ok {
		return fmt.Errorf("Column specified, but not string for restructure operation")
	}

	config := restructure.Config{
		Pattern:      patternStr,
		Variables:    variablesMap,
		Dependencies: dependenciesList,
	}

	length, numThreads, dataChan, errChan, threadNumChan, chunkSize, err := c.getThreadRequirements()
	if err != nil {
		return err
	}

	for i := 0; i < numThreads; i++ {
		size := getAmountOfDataPerThread(i, length, numThreads, chunkSize)
		data := make([]utils.Row, size)

		copy(data, c.data[i*chunkSize:])
		go restructureThread(data, config, columnStr, dataChan, errChan, threadNumChan, i)
	}

	err = getErrors(errChan, numThreads)
	if err != nil {
		return err
	}

	restructuredData := getAndRecombineData(numThreads, threadNumChan, dataChan)

	c.data = restructuredData

	return nil
}

// restructureThread handles the operations of each thread performing a restructure on a subset of the data.
func restructureThread(data []utils.Row, config restructure.Config, columnStr string, dataChan chan []utils.Row, errChan chan error, threadNumChan chan int, threadNum int) {
	restructurer := restructure.New(data)
	err := restructurer.Operate(config, columnStr)

	errChan <- err
	threadNumChan <- threadNum
	dataChan <- data
}

// Validate calls various validate operations on the data
func (c *Controller) Validate(args map[string]interface{}) error {
	action, ok := args["action"]
	if !ok {
		return fmt.Errorf("Action not specified for validate operation")
	}
	actionStr, ok := action.(string)
	if !ok {
		return fmt.Errorf("Action specified, but not string for validate operation")
	}
	validator := validate.New(&c.data)
	validator.ValidateError(actionStr)

	return nil
}

// ValidatePattern validates that every piece of data in a column matches the given pattern
func (c *Controller) ValidatePattern(args map[string]interface{}) error {
	column, ok := args["column"]
	if !ok {
		return fmt.Errorf("Column not specified for validate pattern operation")
	}
	columnStr, ok := column.(string)
	if !ok {
		return fmt.Errorf("Column specified, but not string for validate pattern operation")
	}
	columnInt, err := strconv.Atoi(columnStr)
	if err != nil {
		return fmt.Errorf("Failed to convert column index to int in validate pattern operation")
	}

	action, ok := args["action"]
	if !ok {
		return fmt.Errorf("Action not specified for validate pattern operation")
	}
	actionStr, ok := action.(string)
	if !ok {
		return fmt.Errorf("Action specified, but not string for validate pattern operation")
	}
	pattern, ok := args["pattern"]
	if !ok {
		return fmt.Errorf("Pattern not specified for validate pattern operation")
	}
	patternStr, ok := pattern.(string)
	if !ok {
		return fmt.Errorf("Pattern specified, but not string for validate pattern operation")
	}

	dependencies, ok := args["dependencies"]
	if !ok {
		return fmt.Errorf("Dependecies not specified for validate pattern operation")
	}

	dependenciesInterfaceList, ok := dependencies.([]interface{})
	if !ok {
		return fmt.Errorf("Dependencies specified, but not list for Validate operation")
	}

	dependenciesList, err := utils.ConvertToStringList(dependenciesInterfaceList)
	if err != nil {
		return fmt.Errorf("Error occured when processing dependencies in Validate: %s", err)
	}

	length, numThreads, dataChan, errChan, threadNumChan, chunkSize, err := c.getThreadRequirements()
	if err != nil {
		return err
	}

	for i := 0; i < numThreads; i++ {
		size := getAmountOfDataPerThread(i, length, numThreads, chunkSize)
		data := make([]utils.Row, size)

		copy(data, c.data[i*chunkSize:])
		go validatePatternThread(data, columnInt, actionStr, patternStr, dependenciesList, dataChan, errChan, threadNumChan, i)
	}

	err = getErrors(errChan, numThreads)
	if err != nil {
		return err
	}

	validatedData := getAndRecombineData(numThreads, threadNumChan, dataChan)

	c.data = validatedData

	return nil
}

// validatePatternThread handles the operations of each individual thread performing a validate pattern operation.
func validatePatternThread(data []utils.Row, columnInt int, actionStr string, patternStr string, dependenciesList []string, dataChan chan []utils.Row, errChan chan error, threadNumChan chan int, threadNum int) {
	validator := validate.New(&data)
	err := validator.ValidatePattern(columnInt, actionStr, patternStr, dependenciesList)
	errChan <- err
	threadNumChan <- threadNum
	dataChan <- data
}

// Export writes the controller's data out to the file specified in args
func (c *Controller) Export(args map[string]interface{}) error {
	path, ok := args["path"]
	if !ok {
		return fmt.Errorf("Path not specified for export operation")
	}
	pathStr, ok := path.(string)
	if !ok {
		return fmt.Errorf("Path specified, but not string for export operation")
	}
	exporter := export.New(pathStr)
	return exporter.Export(c.header, c.data)
}
