package controller

import (
	"errors"
	"fmt"
	"machine-assisted-data-mining/src/utils"
	"reflect"
	"runtime"
	"strings"
	"testing"
)

// TestProcessFile verifies that various files are processed correctly.
func TestProcessFile(t *testing.T) {
	//ProcessFile()
}

func TestLoad(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["path"] = "test_data/input.csv"
	args["start"] = 0
	args["end"] = 0
	err := controller.Load(args)
	if err != nil {
		t.Errorf("Unexpected error: %s", err)
	}
	expected := []utils.Row{
		utils.Row{
			Data:   []string{"a", "b"},
			Errors: []error{},
		},
	}
	if !reflect.DeepEqual(controller.data, expected) {
		t.Errorf("Load test failed.  Expected: %v, Actual: %v", expected, controller.data)
	}
}

func TestLoadNoPath(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	err := controller.Load(args)
	if !strings.Contains(err.Error(), "Path not specified") {
		t.Errorf("Error did not contain expected string.  Actual: %s", err)
	}
}

func TestLoadBadPath(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["path"] = "test_data/nofile.csv"
	err := controller.Load(args)
	if err == nil {
		t.Errorf("Expected error when loading with bad path")
	}
}

func TestConvert(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["type"] = "num.int"
	args["function"] = "IntToFloat"
	args["column"] = "all"
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
	}
	err := controller.Convert(args)
	if err != nil {
		t.Errorf("Unexpected error in convert test: %s", err)
	}
	expected := []utils.Row{
		utils.Row{
			Data:   []string{"1.0", "2.0"},
			Errors: []error{},
		},
	}
	if !reflect.DeepEqual(controller.data, expected) {
		t.Errorf("Convert test failed.  Expected: %v, Actual: %v", expected, controller.data)
	}
}

func TestMoreDataThanCPUs(t *testing.T) {
	controller := New("")
	controller.data = make([]utils.Row, 0)
	args := make(map[string]interface{})
	args["type"] = "num.int"
	args["function"] = "IntToFloat"
	args["column"] = "all"
	numCPU := runtime.NumCPU()
	for i := 0; i < 2*numCPU; i++ {
		controller.data = append(controller.data,
			utils.Row{
				Data:   []string{"1", "2"},
				Errors: []error{},
			})
	}
	err := controller.Convert(args)
	if err != nil {
		t.Errorf("Unexpected error in convert test: %s", err)
	}
	expected := make([]utils.Row, 0)
	for i := 0; i < 2*numCPU; i++ {
		expected = append(expected,
			utils.Row{
				Data:   []string{"1.0", "2.0"},
				Errors: []error{},
			})
	}
	if !reflect.DeepEqual(controller.data, expected) {
		t.Errorf("Convert test failed.  Expected: %v, Actual: %v", expected, controller.data)
	}
}

func TestConvertOneColumn(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["type"] = "num.int"
	args["function"] = "IntToFloat"
	args["column"] = "0"
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
	}
	err := controller.Convert(args)
	if err != nil {
		t.Errorf("Unexpected error in convert test: %s", err)
	}
	expected := []utils.Row{
		utils.Row{
			Data:   []string{"1.0", "2"},
			Errors: []error{},
		},
	}
	if !reflect.DeepEqual(controller.data, expected) {
		t.Errorf("Convert test failed.  Expected: %v, Actual: %v", expected, controller.data)
	}
}

func TestConvertNoType(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["function"] = "IntToFloat"
	args["column"] = "all"
	err := controller.Convert(args)
	if !strings.Contains(err.Error(), "Type not specified") {
		t.Errorf("Error did not contain expected string.  Actual: %s", err)
	}
}

func TestConvertNoFunction(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["type"] = "num.int"
	args["column"] = "all"
	err := controller.Convert(args)
	if !strings.Contains(err.Error(), "Function not specified") {
		t.Errorf("Error did not contain expected string.  Actual: %s", err)
	}
}

func TestConvertNoColumn(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["type"] = "num.int"
	args["function"] = "IntToFloat"
	err := controller.Convert(args)
	if !strings.Contains(err.Error(), "Column not specified") {
		t.Errorf("Error did not contain expected string.  Actual: %s", err)
	}
}

func TestConvertNonIntegerColumn(t *testing.T) {
	controller := New("")
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
	}
	args := make(map[string]interface{})
	args["type"] = "num.int"
	args["function"] = "IntToFloat"
	args["column"] = "sheep"
	err := controller.Convert(args)
	if !strings.Contains(err.Error(), "Unable to convert column index") {
		t.Errorf("Error did not contain expected string.  Actual: %s", err)
	}
}

func TestRestructure(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["pattern"] = "[(] number [,] number [)]"
	args["variables"] = map[interface{}]interface{}{
		"number": "num.any",
	}
	args["dependencies"] = []interface{}{
		"num",
	}
	args["column"] = "all"
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"(1,2)", "(3,4)"},
			Errors: []error{},
		},
	}
	err := controller.Restructure(args)
	if err != nil {
		t.Errorf("Unexpected error in restructure test: %s", err)
	}
	expected := []utils.Row{
		utils.Row{
			Data:   []string{"(1,2)", "(3,4)", "1", "2", "3", "4"},
			Errors: []error{},
		},
	}
	if !reflect.DeepEqual(controller.data, expected) {
		t.Errorf("Restructure test failed.  Expected: %v, Actual: %v", expected, controller.data)
	}
}

func TestRestructureOneColumn(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["pattern"] = "[(] number [,] number [)]"
	args["variables"] = map[interface{}]interface{}{
		"number": "num.any",
	}
	args["dependencies"] = []interface{}{
		"num",
	}
	args["column"] = "0"
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"(1,2)", "(3,4)"},
			Errors: []error{},
		},
	}
	err := controller.Restructure(args)
	if err != nil {
		t.Errorf("Unexpected error in restructure test: %s", err)
	}
	expected := []utils.Row{
		utils.Row{
			Data:   []string{"(1,2)", "(3,4)", "1", "2"},
			Errors: []error{},
		},
	}
	if !reflect.DeepEqual(controller.data, expected) {
		t.Errorf("Restructure test failed.  Expected: %v, Actual: %v", expected, controller.data)
	}
}

func TestRestructureNoPattern(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["variables"] = map[interface{}]interface{}{
		"number": "num.any",
	}
	args["dependencies"] = []interface{}{
		"num",
	}
	args["column"] = "all"
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"(1,2)", "(3,4)"},
			Errors: []error{},
		},
	}
	err := controller.Restructure(args)
	if !strings.Contains(err.Error(), "Pattern not specified") {
		t.Errorf("Error did not contain expected string.  Actual: %s", err)
	}
}

func TestRestructureNoVariables(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["pattern"] = "[(] number [,] number [)]"
	args["dependencies"] = []interface{}{
		"num",
	}
	args["column"] = "all"
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"(1,2)", "(3,4)"},
			Errors: []error{},
		},
	}
	err := controller.Restructure(args)
	if !strings.Contains(err.Error(), "Variables not specified") {
		t.Errorf("Error did not contain expected string.  Actual: %s", err)
	}
}

func TestRestructureNoDependencies(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["pattern"] = "[(] number [,] number [)]"
	args["variables"] = map[interface{}]interface{}{
		"number": "num.any",
	}
	args["column"] = "all"
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"(1,2)", "(3,4)"},
			Errors: []error{},
		},
	}
	err := controller.Restructure(args)
	if !strings.Contains(err.Error(), "Dependencies not specified") {
		t.Errorf("Error did not contain expected string.  Actual: %s", err)
	}
}

func TestRestructureNoColumn(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["pattern"] = "[(] number [,] number [)]"
	args["variables"] = map[interface{}]interface{}{
		"number": "num.any",
	}
	args["dependencies"] = []interface{}{
		"num",
	}
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"(1,2)", "(3,4)"},
			Errors: []error{},
		},
	}
	err := controller.Restructure(args)
	if !strings.Contains(err.Error(), "Column not specified") {
		t.Errorf("Error did not contain expected string.  Actual: %s", err)
	}
}

func TestValidate(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["action"] = "Delete"
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3", "4"},
			Errors: []error{fmt.Errorf("test error")},
		},
	}
	err := controller.Validate(args)
	if err != nil {
		t.Errorf("Unexpected error in validate: %s", err)
	}
	expected := []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
		utils.Row{
			Data:    []string{"3", "4"},
			Errors:  []error{fmt.Errorf("test error")},
			Deleted: true,
		},
	}
	if !reflect.DeepEqual(controller.data, expected) {
		t.Errorf("Validate test failed.  Expected: %v, Actual: %v", expected, controller.data)
	}
}

func TestValidateNoAction(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3", "4"},
			Errors: []error{fmt.Errorf("test error")},
		},
	}
	err := controller.Validate(args)
	if !strings.Contains(err.Error(), "Action not specified") {
		t.Errorf("Error did not contain expected string.  Actual: %s", err)
	}
}

func TestValidatePattern(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["action"] = "Delete"
	args["column"] = "0"
	args["pattern"] = "^num.int$"
	args["dependencies"] = []interface{}{
		"num", "all",
	}
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"(1,2)", "2"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3", "4"},
			Errors: []error{},
		},
	}
	err := controller.ValidatePattern(args)
	if err != nil {
		t.Errorf("Unexpected error in validate: %s", err)
	}
	expected := []utils.Row{
		utils.Row{
			Data: []string{"(1,2)", "2"},
			Errors: []error{
				errors.New("Pattern does not match"),
			},
			Deleted: true,
		},
		utils.Row{
			Data:   []string{"3", "4"},
			Errors: []error{},
		},
	}
	if !reflect.DeepEqual(controller.data, expected) {
		t.Errorf("Validate test failed.  Expected: %v, Actual: %v", expected, controller.data)
	}
}

func TestValidatePatternNoAction(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["column"] = "0"
	args["pattern"] = "^num.int$"
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3", "4"},
			Errors: []error{fmt.Errorf("test error")},
		},
	}
	err := controller.ValidatePattern(args)
	if !strings.Contains(err.Error(), "Action not specified") {
		t.Errorf("Error did not contain expected string.  Actual: %s", err)
	}
}

func TestValidatePatternNoColumn(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["action"] = "Delete"
	args["pattern"] = "^num.int$"
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3", "4"},
			Errors: []error{fmt.Errorf("test error")},
		},
	}
	err := controller.ValidatePattern(args)
	if !strings.Contains(err.Error(), "Column not specified") {
		t.Errorf("Error did not contain expected string.  Actual: %s", err)
	}
}

func TestValidatePatternNoPattern(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["action"] = "Delete"
	args["column"] = "0"
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3", "4"},
			Errors: []error{fmt.Errorf("test error")},
		},
	}
	err := controller.ValidatePattern(args)
	if !strings.Contains(err.Error(), "Pattern not specified") {
		t.Errorf("Error did not contain expected string.  Actual: %s", err)
	}
}
func TestExport(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["path"] = "test_data/output.csv"
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
	}
	err := controller.Export(args)
	if err != nil {
		t.Errorf("Unexpected error in Export: %s", err)
	}
	loadController := New("")
	loadController.Load(args)
	if !reflect.DeepEqual(loadController.data, controller.data) {
		t.Errorf("Export test failed.  Expected: %v, Actual: %v", controller.data, loadController.data)
	}
}

func TestExportNoPath(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	err := controller.Export(args)
	if !strings.Contains(err.Error(), "Path not specified") {
		t.Errorf("Error did not contain expected string.  Actual: %s", err)
	}
}

func TestProcess(t *testing.T) {
	controller := New("test_data/config.yaml")
	err := controller.Process()
	if err != nil {
		t.Errorf("Unexpected error in Process: %s", err)
	}
	args := make(map[string]interface{})
	args["path"] = "test_data/process_output.csv"
	verifyController := New("bad-config")
	verifyController.Load(args)
	expected := []utils.Row{
		utils.Row{
			Data:   []string{"1.0", "2.0"},
			Errors: []error{},
		},
	}
	if !reflect.DeepEqual(verifyController.data, expected) {
		t.Errorf("Export test failed.  Expected: %v, Actual: %v", expected, verifyController.data)
	}
}

func TestProcessNonExistentConfig(t *testing.T) {
	controller := New("test_data/no_config.yaml")
	err := controller.Process()
	if err == nil {
		t.Errorf("Expected error when loading non-existent config file")
	}
}

func TestProcessBadConfig(t *testing.T) {
	controller := New("test_data/bad_config.yaml")
	err := controller.Process()
	if !strings.Contains(err.Error(), "Non-existent operation specified") {
		t.Errorf("Error doesn't contain expected information.  Actual: %s", err)
	}
}

func TestConfigBadArgument(t *testing.T) {
	controller := New("test_data/config_bad_convert.yaml")
	err := controller.Process()
	if err == nil {
		t.Errorf("Expected error when passing invalid argument to convert")
	}
}

func TestDontRecordConvertErrors(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["type"] = "num.int"
	args["function"] = "IntToFloat"
	args["column"] = "0"
	args["record_format_errors"] = false
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3.0", "2"},
			Errors: []error{},
		},
	}
	err := controller.Convert(args)
	if err != nil {
		t.Errorf("Unexpected error in convert test: %s", err)
	}
	expected := []utils.Row{
		utils.Row{
			Data:   []string{"1.0", "2"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3.0", "2"},
			Errors: []error{},
		},
	}
	if !reflect.DeepEqual(controller.data, expected) {
		t.Errorf("Convert test failed.  Expected: %v, Actual: %v", expected, controller.data)
	}
}

func TestRecordConvertErrors(t *testing.T) {
	controller := New("")
	args := make(map[string]interface{})
	args["type"] = "num.int"
	args["function"] = "IntToFloat"
	args["column"] = "0"
	args["record_format_errors"] = true
	controller.data = []utils.Row{
		utils.Row{
			Data:   []string{"1", "2"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3.0", "2"},
			Errors: []error{},
		},
	}
	err := controller.Convert(args)
	if err != nil {
		t.Errorf("Unexpected error in convert test: %s", err)
	}
	expected := []utils.Row{
		utils.Row{
			Data:   []string{"1.0", "2"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"3.0", "2"},
			Errors: []error{errors.New("Type did not match in column: 0")},
		},
	}
	if !reflect.DeepEqual(controller.data, expected) {
		t.Errorf("Convert test failed.  Expected: %v, Actual: %v", expected, controller.data)
	}
}
