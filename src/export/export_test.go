package export

import (
	"machine-assisted-data-mining/src/load"
	"machine-assisted-data-mining/src/testutils"
	"machine-assisted-data-mining/src/utils"
	"testing"
)

func TestExport(t *testing.T) {
	exporter := New("test_export.csv")
	header := utils.Row{
		Data:   []string{"number, number"},
		Errors: []error{},
	}
	data := []utils.Row{
		utils.Row{
			Data:   []string{"1", "2", "3"},
			Errors: []error{},
		},
		utils.Row{
			Data:   []string{"4", "5", "6", "7"},
			Errors: []error{},
		},
	}
	err := exporter.Export(header, data)
	if err != nil {
		t.Errorf("Unexpected error while exporting csv: %s", err)
	}

	loader := load.New("test_export.csv")
	dataWritten, err := loader.Load()
	if err != nil {
		t.Errorf("Unexpected error while reading csv: %s", err)
	}

	expectedData := make([]utils.Row, 0)
	expectedData = append(expectedData, header)
	expectedData = append(expectedData, data...)
	if !testutils.RowDataEqual(expectedData, dataWritten) {
		t.Errorf("File contents not equal to expected.")
	}
}
