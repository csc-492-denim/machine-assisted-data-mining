package export

import (
	"encoding/csv"
	"machine-assisted-data-mining/src/utils"
	"os"
)

// Exporter provides an interface for exporting a slice of rows to a file.
type Exporter interface {
	Export(utils.Row, []utils.Row) error
}

type csvExporter struct {
	path string
}

// New returns an exporter based on the file type.
func New(path string) Exporter {
	return csvExporter{path: path}
}

// Export exports a slice of rows to a CSV file.
func (c csvExporter) Export(header utils.Row, data []utils.Row) error {
	// https://golangcode.com/write-data-to-a-csv-file/
	path := c.path

	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()

	writer := csv.NewWriter(file)
	defer writer.Flush()

	if len(header.Data) > 0 {
		err := writer.Write(header.Data)
		if err != nil {
			header.AddError(err)
		}
	}
	for _, value := range data {
		if value.Deleted {
			continue
		}
		err := writer.Write(value.Data)
		if err != nil {
			value.AddError(err)
		}
	}

	return nil
}
