package load

import (
	"machine-assisted-data-mining/src/testutils"
	"machine-assisted-data-mining/src/utils"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

// TestLoadCSV is a test function to test csv loader
func TestLoadCSV(t *testing.T) {
	load := New("test_data/test1.csv")
	result, err := load.Load()
	//To check if the file exist
	assert.Equal(t, err, nil)
	expectedResult := []utils.Row{}
	expectedResult = testutils.InsertRowWithString(expectedResult, []string{"1", "2", "3", "4"})
	expectedResult = testutils.InsertRowWithString(expectedResult, []string{"a", "b", "c", "d"})
	expectedResult = testutils.InsertRowWithString(expectedResult, []string{"", "", "", ""})
	expectedResult = testutils.InsertRowWithString(expectedResult, []string{"1", "2", "3", "4"})
	expectedResult = testutils.InsertRowWithString(expectedResult, []string{"a", "b", "c", "d"})
	expectedResult = testutils.InsertRowWithString(expectedResult, []string{"", "", "", ""})

	for _, row := range expectedResult {
		assert.Equal(t, testutils.ContainString(result, row.Data), true)
	}
	assert.Equal(t, len(result), len(expectedResult))
	assert.Equal(t, testutils.CheckErrors(result), false)

	result, err = load.LoadStart(1)
	expectedResultFromOne := expectedResult[1:]
	for _, row := range expectedResult {
		assert.Equal(t, testutils.ContainString(result, row.Data), true)
	}
	assert.Equal(t, len(result), len(expectedResultFromOne))
	assert.Equal(t, testutils.CheckErrors(result), false)
	result, err = load.LoadStartEnd(2, 3)
	expectedResult = expectedResult[2:4]

	for _, row := range expectedResult {
		assert.Equal(t, testutils.ContainString(result, row.Data), true)
	}

	assert.Equal(t, len(result), len(expectedResult))
	assert.Equal(t, testutils.CheckErrors(result), false)

	assert.Equal(t, expectedResult[0].LineNumber, result[0].LineNumber)
}

func TestLoadInvalidUTF8(t *testing.T) {
	load := New("test_data/test2.csv")
	result, err := load.Load()
	assert.Equal(t, err, nil)
	if !strings.Contains(result[0].Errors[0].Error(), "Invalid UTF8") {
		t.Errorf("Expected UTF8 error when loading image as CSV file.")
	}
}
