package load

//Author: Edward Chan
import (
	"encoding/csv"
	"errors"
	"io"
	"machine-assisted-data-mining/src/utils"
	"os"
	"unicode/utf8"
)

/*
	Loader interface provides a generic file parser class
*/
type Loader interface {
	Load() ([]utils.Row, error)
	LoadStart(int) ([]utils.Row, error)
	LoadStartEnd(int, int) ([]utils.Row, error)
	LoadEnd(int) ([]utils.Row, error)
}

/*
	csvLoader is a file parser for csv file type
	is a Loader
*/
type csvLoader struct {
	path string
}

/*
	New returns an appropriate loader based on the file extension
	TODO: add path parser to determine if a file is csv
*/
func New(path string) Loader {
	return csvLoader{path: path}
}

// Load is a function which loads a csv from beginning to the end.
func (csvL csvLoader) Load() (rows []utils.Row, err error) {
	return csvL.LoadStartEnd(0, -99)
}

// LoadStart loads from a specified beinnings to the end
func (csvL csvLoader) LoadStart(starts int) (rows []utils.Row, err error) {
	return csvL.LoadStartEnd(starts, -99)
}

// LoadEnd loads csv file to a speicfic end
func (csvL csvLoader) LoadEnd(end int) (rows []utils.Row, err error) {
	return csvL.LoadStartEnd(0, end)
}

/*
	Load is the Load function for csvValidator.
	It returns a 2d slice of string and a slice of error
	data contains data with rows and columns
	err contatins all the error on load. Each error is an element
*/
func (csvL csvLoader) LoadStartEnd(starts int, ends int) (rows []utils.Row, err error) {
	path := csvL.path
	//opening file
	file, ferror := os.Open(path)
	if ferror != nil {
		return nil, ferror
	}
	defer file.Close()
	//parsing file per line
	reader := csv.NewReader(file)
	for lineNumber := 0; ; lineNumber++ {
		line, ioerror := reader.Read()
		if lineNumber < starts {
			continue
		}

		if ioerror == io.EOF || lineNumber == ends+1 {
			return rows, nil
		}

		if _, ok := ioerror.(*csv.ParseError); ok {
			// if ioerror is a csv error and not EOF, then add to error stack of row and continue
			rows = append(rows, utils.Row{Data: line, Errors: []error{ioerror}, LineNumber: lineNumber})
			continue
		}

		if ioerror != nil {
			// any other type of error, we've hit a problem, return to controller.
			return nil, ioerror
		}
		// now we need to see if the line is valid utf8.
		if !validUtf8(line) {
			utf8err := errors.New("Invalid UTF8 in line")
			rows = append(rows, utils.Row{Data: line, Errors: []error{utf8err}, LineNumber: lineNumber})
			continue
		}
		rows = append(rows, utils.Row{Data: line, Errors: []error{}, LineNumber: lineNumber})
	}
}

func validUtf8(data []string) bool {
	for _, value := range data {
		if !utf8.ValidString(value) {
			return false
		}
	}
	return true
}
